<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();        
    }
}

class Admin_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();   
        if (!$this->session->userdata('is_login') || $this->session->userdata('status') != 'admin')        
        {
            $this->session->set_flashdata('test', 'ANDA BUKAN ADMINISTRATOR');
            redirect('admin/login');
        }
    }
}

class Dosen_Controller extends CI_Controller {

	function __construct()
    {
        parent::__construct();     
        if ($this->session->userdata('is_dosen_login') != true || $this->session->userdata('status') != 'dosen')        
        {
            $this->session->set_flashdata('test', 'ANDA BUKAN DOSEN');
            redirect('login');
        }
    }
}