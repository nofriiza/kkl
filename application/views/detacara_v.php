      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detail Acara <?php echo $detail->judul ?> (<?php echo $detail->mulai ?> - <?php echo $detail->selesai ?>)
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Acara</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Detail Peserta</h3>
                  <div class="box-tools">
                    <?php echo form_open('acara/caridetail/'.$detail->id); ?>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="cari_acaradet" placeholder="NPP" class="form-control input-sm pull-right" value="<?php echo $this->session->userdata('cari_acaradet'); ?>">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                    <?php echo form_close(); ?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Nama Peserta</th>
                      <th>Keterangan</th>
                    </tr>
                    <?php 
                    $no = $this->uri->segment(4) ? $this->uri->segment(4)+1 : 1;
                    /*$sekarang = new DateTime("now");
                    $mulai = new DateTime($detail->mulai);
                    $selesai = new DateTime($detail->selesai);*/
                    foreach ($acara->result() as $rows) { 
                    ?>                    
                    <tr>
                      <td><?php echo $no ?></td>                      
                      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
                      <td>
                        <?php if(date("Y-m-d H:i:s") < $rows->mulai){ ?>
                        Acara Belum Dimulai
                        <?php } else if($rows->nilai > 0){ ?>
                        Hadir
                        <?php } else if($rows->nilai == 0){ ?>
                        Tidak Hadir
                        <?php } ?>

                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  Total Rows : <?php echo $num_rows ?>
                </div>
                
                <?php echo $pagination ?>
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
        function cekabsen() {
          if(confirm("Peserta Sudah Datang ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>