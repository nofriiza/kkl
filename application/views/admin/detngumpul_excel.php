<?php 
$filename = "List Peserta ".$ngumpul->judul;
header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
header("Content-Disposition: attachment;filename='$filename.xlsx'");
header("Cache-Control: max-age=0"); 
?>
<style type="text/css">
table {
  width: 100%;
  border-collapse: collapse;
}

table, th, td {
  padding: 5px;
  border: 1px solid black;
}
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm">
      <h1>
        <?php echo $ngumpul->judul ?>
      </h1>

      <?php if($this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->mk == 'ya'){ ?>
      <?php echo "Jenis : ".$this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->nama_jenis ?>
      <?php } else { ?>
      <?php if(date("Y-m-d") >= $ngumpul){ ?>
      <h3 class="box-title"><a class="btn btn-primary" href="<?php echo site_url('admin/ngumpul/adddetail/'.$ngumpul->id) ?>">Tambah Peserta</a></h3>
      <?php } else { ?>
      <?php echo "Jenis : ".$this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->nama_jenis ?>
      <?php } ?>
      <?php } ?>

      <table class="tabel">
      <?php $total = 0; if($this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->mk == 'ya'){ ?>
        <tr>
          <th>No</th>
          <th>Nama Dosen</th>
          <th>Tgl Dikumpul</th>
          <th>Nama Matakuliah</th>
          <th>Nilai</th>
          <th>Action</th>
        </tr>
        <?php 
        $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
        foreach ($mk->result() as $rows) { 
          $detngumpul = $this->ngumpul_m->get_one_data($ngumpul->id,$rows->npp,$rows->kodemk);
          if(@$detngumpul->nilai){
            $total++;
          }
        ?>                    
        <tr>
          <td><?php echo $no ?></td>                      
          <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
          <td><?php
            if (!@$detngumpul->tgl_dikumpul) { 
              echo "Peserta Belum Mengumpulkan"; 
            } else { 
              echo $detngumpul->tgl_dikumpul;
            } ?>
          </td>
          <td><?php $mk = $this->libdb->get_mk($rows->kodemk); echo $mk->namamk . " (" . $mk->kodemk . ")" ?></td>
          <td><?php echo @$detngumpul->nilai ?></td>
          <td>
            <?php if(@!$detngumpul->tgl_dikumpul){ ?>
            <a class="btn btn-info" onclick="return cekabsen()" href="<?php echo site_url('admin/ngumpul/soalnilai/'.$this->uri->segment(4)."/".$rows->npp)."/".$rows->kodemk ?>">Tandai Mengumpulkan</a>
            <?php } else { ?>
              Peserta Sudah Mengumpulkan
            <?php } ?>
          </td>
        </tr>
        <?php $no++; } ?>
      <?php } else { ?>
        <tr>
          <th>No</th>
          <th>Nama Dosen</th>
          <th>Tgl Dikumpul</th>
          <th>Nilai</th>
          <th>Keterangan</th>
        </tr>
        <?php 
        $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
        foreach ($detngumpul->result() as $rows) { 
          if($rows->nilai){
            $total++;
          }
        ?>                    
        <tr>
          <td><?php echo $no ?></td>                      
          <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
          <td><?php
            if (@$rows->tgl_dikumpul == '0000-00-00') { 
              echo "Peserta Belum Mengumpulkan"; 
            } else { 
              echo $rows->tgl_dikumpul;
            } ?>
          </td>
          <td><?php echo $rows->nilai ?></td>
          <td><?php echo $rows->keterangan ?></td>
        </tr>
        <?php $no++; } ?>
        <?php } ?>
      </table>

    
      Total Peserta : <?php echo $num_rows ?><br />
      Total Sudah Mengumpulkan : <?php echo $total_ngumpul ?><br />
      Deadline : <?php echo $ngumpul->waktu ?>
</page>