      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Nama Kepanitiaan : <?php echo $kepanitiaan->nama_kepanitiaan ?> (<?php echo $this->libdb->get_one_dosen($kepanitiaan->ketua)->nama ?>)
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Kepanitiaan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header" style="margin-bottom: 10px">
                  <h3 class="box-title" >Tanggal SK : <?php echo tgl_indo($kepanitiaan->tgl_sk) ?><br/>
                    <?php if($this->session->userdata('npp') == $kepanitiaan->ketua || $this->session->userdata('npp') == 'admins'){ ?>
                    <a href="<?php echo site_url('admin/kepanitiaan/tambahdetail/'.$kepanitiaan->id) ?>" class="btn btn-info" style="margin-top: 10px">Tambah</a>
                    <?php } ?>
                  </h3>
                  <div class="box-tools" style="float:right;padding-bottom: 20px">
                    <!-- <a href="LinkPrint / Bisa dihilangkan aja" class="btn btn-info" title="Print"><i class="fa fa-print"></i></a> -->
                    <a href="<?php echo site_url('admin/kepanitiaan/detail_excel/'.$kepanitiaan->id) ?>" class="btn btn-success" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
                    <a href="<?php echo site_url('admin/kepanitiaan/detail_pdf/'.$kepanitiaan->id)?>" target="_blank" class="btn btn-danger" title="Preview / Download PDF"><i class="fa fa-file-pdf-o"></i></a>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                  <?php $total = 0; ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Peserta</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                      <?php if($this->session->userdata('npp') == $kepanitiaan->ketua || $this->session->userdata('npp') == 'admins'){ ?>
                      <th>Action</th>
                      <?php } ?>
                    </tr>
                    <?php 
                    $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
                    foreach ($detkepanitiaan->result() as $rows) { 
                      if($rows->nilai){
                        $total++;
                      }
                    ?>                    
                    <tr>
                      <td><?php echo $no ?></td>                      
                      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                      <?php if($this->session->userdata('npp') == $kepanitiaan->ketua || $this->session->userdata('npp') == 'admins'){ ?>
                      <td><a href="<?php echo site_url('admin/kepanitiaan/editdetail/'.$rows->id) ?>" style="margin-right: 3px" class="btn btn-info">Edit</a><a href="<?php echo site_url('admin/kepanitiaan/hapusdetail/'.$rows->id) ?>" onclick="return cek()" class="btn btn-danger" style="margin-right: 3px">Hapus</a>
                      <?php } ?>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  Total Peserta : <?php echo $num_rows ?><br />
                </div>
                
                <?php echo $pagination ?>
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>