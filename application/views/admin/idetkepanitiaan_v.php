      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Peserta Kepanitiaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/kepanitiaan') ?>">Kepanitiaan</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'tambahdetail' ? "Tambah" : "Edit" ?> Data Peserta Kepanitiaan</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/kepanitiaan/simpandetail', array('id' => 'kepanitiaan-form')); ?>
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <input type="hidden" name="idkepanitiaan" value="<?php echo @$kepanitiaan->id ?>">
                  <input type="hidden" name="iddetkepanitiaan" value="<?php echo @$detkepanitiaan->id ?>">
                  <div class="form-group" id="semua">
                    <label>Peserta Pekepanitiaanan</label>
                    <select class="form-control select2" name="npp" data-placeholder="Pilih Peserta Pekepanitiaanan" style="width: 100%;">
                      <?php foreach ($peserta->result() as $rows) { ?>
                        <option <?php echo (@$detkepanitiaan->npp == $rows->npp) ? 'selected' : ''; ?> value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div><!-- /.form-group -->
                  <?php if($this->uri->segment(3) == 'editdetail'){ ?>
                  <div class="form-group" id="semua">
                    <label>Nilai</label>
                    <input type="number" class="form-control" min='0' max='5' name="nilai" value="<?php echo $detkepanitiaan->nilai ?>"></input>
                  </div><!-- /.form-group -->
                  <?php } ?>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="keterangan" placeholder="Jabatan Kepanitiaan"><?php echo @$detkepanitiaan->keterangan ?></textarea>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#kepanitiaan-form").validate({
            rules:{
              id_jenis : "required",
              judul : "required",
              "npp[]" : "required",
              waktu : "required"
            },
            messages:{
              id_jenis : "<p class='text-red'>Field tidak boleh kosong</p>",
              judul : "<p class='text-red'>Field tidak boleh kosong</p>",
              "npp[]" : "<p class='text-red'>Field tidak boleh kosong</p>",
              waktu : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          });

        });
      </script>