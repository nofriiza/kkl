      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            List Acara 
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Acara</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><a class="btn btn-primary" href="<?php echo site_url('admin/acara/add') ?>">Tambah Data</a></h3>
                  <div class="box-tools">
                    <?php echo form_open('admin/acara/cari'); ?>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="cari_acara" class="form-control input-sm pull-right" value="<?php echo $this->session->userdata('cari_acara'); ?>">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                    <?php echo form_close(); ?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Judul Acara</th>
                      <th>Jenis Acara</th>
                      <th>Mulai<!--  - Selesai --></th>
                      <th>Tempat</th>
                      <th>Status</th>
                      <th>Thajaran</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                    </tr>
                    <?php $no = 1;foreach ($acara->result() as $rows) { ?>                    
                    <tr>
                      <td><?php echo $no ?></td>
                      <td><?php echo $rows->judul ?></td>
                      <td><?php echo $this->libdb->get_one_jenis_acara($rows->id_jenis)->nama_jenis ?></td>
                      <td><?php echo tgl_indo($rows->mulai)?></td>
                      <td><?php echo $rows->tempat ?></td>
                      <td>
                        <?php if(date("Y-m-d H:i:s") < $rows->mulai){ ?>
                          Acara Belum Dimulai
                        <?php } else if(date("Y-m-d H:i:s") >= $rows->mulai && date("Y-m-d H:i:s") <= $rows->selesai && $rows->jam_diakhiri == "0000-00-00 00:00:00" ) { ?>
                          Acara Sedang Berlangsung
                        <?php } else if(date("Y-m-d H:i:s") > $rows->selesai || $rows->jam_diakhiri != "0000-00-00 00:00:00" ) { ?>
                          Acara Sudah Selesai
                        <?php } ?>
                      </td>
                      <td><?php echo @$rows->thajaran ?></td>
                      <td><?php echo word_limiter($rows->keterangan,5) ?></td>
                      <td>
                        <?php 
                          $batas = new DateTime($rows->mulai);
                          $batas->modify("+30 minutes");
                        ?>
                        <?php if(($rows->created_by == $this->session->userdata('npp') || $this->session->userdata('npp') == 'admins') && (date("Y-m-d H:i:s") < $rows->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && $rows->jam_diakhiri != '0000-00-00 00:00:00'){ ?>
                        <a class="btn btn-success" href="<?php echo site_url('admin/acara/edit/'.$rows->id) ?>">Edit</a>
                        <?php } ?>
                        <a class="btn btn-info" href="<?php echo site_url('admin/pesertaacara/detail/'.$rows->id) ?>">Detail</a>
                        <?php if(($rows->created_by == $this->session->userdata('npp') || $this->session->userdata('npp') == 'admins') && (date("Y-m-d H:i:s") < $rows->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && $rows->jam_diakhiri != '0000-00-00 00:00:00'){ ?>
                        <a class="btn btn-danger" onclick="return cek()" href="<?php echo site_url('admin/acara/hapus/'.$rows->id) ?>">Hapus</a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  Total Rows : <?php echo $num_rows ?>
                </div>
                
                <?php echo $pagination ?>
                <!-- <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div> -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>