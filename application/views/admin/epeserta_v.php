      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tidak Bisa Hadir
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/acara') ?>">Acara</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Keterangan</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/pesertaacara/save', array('id' => 'acara-form')); ?>
            <div class="box-body">
              <div class="row">
              <!-- <?php 
                $acara = $this->libdb->get_one_acara($this->session->userdata('idacara'));
                $mulai = new DateTime($acara->mulai);
                $sekarang = new DateTime(date("Y-m-d H:i:s"));
                echo $sekarang->format('Y-m-d H:i:s')."<br />";
                echo $mulai->format('Y-m-d H:i:s')."<br />";

                echo $mulai->diff($sekarang)->format('%Y-%M-%D %H:%I:%S');
              ?> -->

                <div class="col-md-6">
                  <input type="hidden" name="id_peserta" value="<?php echo @$peserta->id ?>">
                  <?php if($this->uri->segment(3) == 'edit'){ ?>
                  <input type="hidden" name="edit" value="ya">
                  <?php } ?>
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input class="form-control" readonly value="<?php echo $this->libdb->get_one_dosen($peserta->npp)->nama ?>">
                  </div>
                  <?php if($this->uri->segment(3) == 'edit'){ ?>
                  <div class="form-group">
                    <label>Jam Datang</label>
                    <input class="form-control" name="jam_datang" id="waktu-mulai" value="<?php echo @$peserta->jam_datang ?>">
                  </div>
                  <?php } ?>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <?php /*if($this->uri->segment(3) == 'edit'){ ?>
                  <div class="form-group">
                    <label>Nilai</label>
                    <input class="form-control" name="nilai" value="<?php echo @$peserta->nilai ?>">
                  </div>
                  <?php }*/ ?>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="keterangan"><?php echo @$peserta->keterangan ?></textarea>
                  </div>
                  <!-- <div class="form-group">
                    <label>Nilai</label>
                    <input type="text" class="form-control" name="nilai">
                  </div> -->
                </div><!-- /.col -->

                
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#acara-form").validate({
            rules:{
              keterangan : "required"
            },
            messages:{
              keterangan : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>