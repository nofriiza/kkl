      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            List Peserta
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Acara</li>
          </ol>
        </section>
        <?php 
        $batas = new DateTime($acara->mulai);
        $batas->modify("+30 minutes");
        ?>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <?php if(date("Y-m-d H:i:s") < $acara->mulai || (date("Y-m-d H:i:s") < $acara->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && $acara->jam_diakhiri == '0000-00-00 00:00:00'){ ?>
                  <h3 class="box-title"><a class="btn btn-primary" href="<?php echo site_url('admin/pesertaacara/add/') ?>">Tambah Peserta</a>
                  <?php } else { ?>
                  <h3 class="box-title"><?php echo $acara->judul ?>
                  <?php } ?>
                  <b><?php echo $acara->mulai ?></b> Sampai <b><?php echo ($acara->jam_diakhiri != '0000-00-00 00:00:00') ? $acara->jam_diakhiri : $acara->selesai ?></b></h3>
                  <?php if($acara->jam_diakhiri != '0000-00-00 00:00:00' || date('Y-m-d H:i:s') > $acara->selesai){ ?>
                  <div class="box-tools" style="float:right;padding-bottom: 20px">
                    <!-- <a href="LinkPrint / Bisa dihilangkan aja" class="btn btn-info" title="Print"><i class="fa fa-print"></i></a> -->
                    <a href="<?php echo site_url('admin/pesertaacara/detail_excel/'.$acara->id) ?>" class="btn btn-success" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
                    <a href="<?php echo site_url('admin/pesertaacara/detail_pdf/'.$acara->id)?>" target="_blank" class="btn btn-danger" title="Preview / Download PDF"><i class="fa fa-file-pdf-o"></i></a>
                  </div>
                  <?php } ?>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Nama Peserta</th>
                      <th>Jam Datang</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                    </tr>
                    <?php 
                    $no = 1;
                    /*$sekarang = new DateTime("now");
                    $mulai = new DateTime($acara->mulai);
                    $selesai = new DateTime($acara->selesai);*/
                    foreach ($peserta->result() as $rows) { 
                    ?>                    
                    <tr>
                      <td><?php echo $no ?></td>                      
                      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>

                      <!-- <td><?php echo $rows->jam_datang ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td> -->
                      <td><?php
                        if(substr($rows->keterangan,0,4) == 'Izin'){
                          echo "Peserta Izin";
                        } elseif($rows->keterangan == 'Tidak Hadir Tanpa Keterangan' || (date('Y-m-d H:i:s') < $batas && $rows->nilai == '0')){
                          echo "Peserta tidak hadir";
                        } elseif($rows->jam_datang != "0000-00-00 00:00:00"){
                          echo $rows->jam_datang;
                        } elseif(date("Y-m-d H:i:s") < $acara->mulai) { 
                          echo "Acara Belum Dimulai"; 
                        } elseif ($rows->jam_datang == "0000-00-00 00:00:00") { 
                          echo "Peserta Belum Datang"; 
                        } else { 
                          echo $rows->jam_datang;
                        } ?>
                      </td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                      <td>
                        <?php 
                          
                          //echo (date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s") ) ? 'ya' : 'tidak';
                          /*if(date("Y-m-d H:i:s") >= $acara->mulai && 
                              $rows->jam_datang == "0000-00-00 00:00:00" || 
                              (date("Y-m-d H:i:s") < $acara->selesai && 
                              date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && 
                              $rows->jam_datang == "0000-00-00 00:00:00" ){
                            echo "ya";
                          }*/
                        ?>

                        <?php if(date("Y-m-d H:i:s") < $acara->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s") && !$rows->keterangan && $rows->jam_datang == "0000-00-00 00:00:00"){ ?>
                        <a class="btn btn-info" href="<?php echo site_url('admin/pesertaacara/ijin/'.$rows->id) ?>">Tidak bisa hadir</a>
                        <?php } ?>

                        <?php 
                        if(date("Y-m-d H:i:s") < $acara->selesai ){
                          if(date("Y-m-d H:i:s") >= $acara->mulai && $rows->jam_datang == "0000-00-00 00:00:00" || (date("Y-m-d H:i:s") < $acara->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && $rows->jam_datang == "0000-00-00 00:00:00" ) { ?>
                        <a class="btn btn-info" onclick="return cekabsen()" href="<?php echo site_url('admin/pesertaacara/absen/'.$rows->id) ?>">Absen Peserta</a>
                        <?php } } ?>

                        <?php if((date("Y-m-d H:i:s") < $acara->selesai && date("Y-m-d H:i:s") < $batas->format("Y-m-d H:i:s")) && ($acara->jam_diakhiri == '0000-00-00 00:00:00' || date("Y-m-d H:i:s") >= $acara->selesai)){ ?>
                        <a class="btn btn-danger" onclick="return cek()" href="<?php echo site_url('admin/pesertaacara/hapus/'.$rows->id) ?>">Hapus</a>
                        <?php } ?>

                        <?php if(date("Y-m-d H:i:s") >= $acara->selesai || $acara->jam_diakhiri != '0000-00-00 00:00:00'){ ?>
                        <a class="btn btn-success" href="<?php echo site_url('admin/pesertaacara/edit/'.$rows->id) ?>">Edit</a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php if(date("Y-m-d H:i:s") > $acara->mulai){ ?>
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Catatan Notulen</h3>
                  <!-- tools box -->
                  <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div><!-- /. tools -->
                </div><!-- /.box-header -->
                <?php echo form_open('admin/pesertaacara/selesai'); ?>
                <div class="box-body pad">                  
                    <input type="hidden" name="id_acara" value="<?php echo $acara->id ?>">
                    <input type="hidden" name="jam_diakhiri" value="<?php echo $acara->jam_diakhiri ?>">
                    <textarea class="editor" name="notulensi" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $acara->notulen ?></textarea>
                </div>
                <div class="box-footer">
                  <button type="submit" onclick="return cek_selesai()" class="btn btn-primary">Submit</button>
                </div>
                <?php echo form_close(); ?>
              </div>
              <?php } else if(date("Y-m-d H:i:s") > $acara->selesai) { ?>
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-text-width"></i>
                  <h3 class="box-title">Catatan Notulen</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <h4><?php echo $acara->notulen ?></h4>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              <?php } ?>
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script src="<?php echo base_url()?>asset/admin/depan/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" ></script>
      <script type="text/javascript">
        $(document).ready(function() {
          $(".editor").wysihtml5();
        });
      </script>
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
        function cek_selesai() {
          if(confirm("Yakin Acara Sudah Selesai ?") == true){
            return true;
          } else {
            return false;
          }
        }
        function cekabsen() {
          if(confirm("Peserta Sudah Datang ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>