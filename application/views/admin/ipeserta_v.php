      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Peserta
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/acara') ?>">Acara</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Peserta</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/pesertaacara/simpan', array('id' => 'acara-form')); ?>
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <input type="hidden" name="id_acara" value="<?php echo @$acara->id ?>">
                  <div class="form-group">
                    <label>Nama Acara</label>
                    <input type="text" class="form-control" readonly value="<?php echo $acara->judul ?>">
                  </div>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <input type="hidden" name="idacara" value="<?php echo @$acara->id ?>">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <select class="form-control select2" name="npp" style="width: 100%;">
                      <option value="">Pilih Dosen</option>
                      <?php foreach ($dosen->result() as $rows) { ?>
                      <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#acara-form").validate({
            rules:{
              npp : "required"
            },
            messages:{
              npp : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>