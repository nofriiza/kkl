      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Jenis Ngumpul
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/jenisngumpul') ?>">Jenis Ngumpul</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Jenis</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <?php echo form_open('admin/jenisngumpul/simpan', array('id' => 'jenis-form')); ?>
                <div class="col-md-6">
                  <input type="hidden" name="idjenisngumpul" value="<?php echo @$jenisngumpul->id ?>">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Jenis</label>
                    <input type="text" name="nama_jenis" class="form-control" id="nama_jenis" value="<?php echo @$jenisngumpul->nama_jenis ?>">
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </div><!-- /.col -->
                <div class="col-md-6">
                  <input type="hidden" name="idjenisngumpul" value="<?php echo @$jenisngumpul->id ?>">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pengumpulan dengan Matakuliah</label>
                    <div class="radio">
                      <label>
                        <input type="radio" name="mk" <?php echo (@$jenisngumpul->mk == 'tidak') ? 'checked' : '' ?> value="tidak">
                        Tidak
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="mk" <?php echo (@$jenisngumpul->mk == 'ya') ? 'checked' : '' ?> value="ya">
                        Ya
                      </label>
                    </div>
                    
                  </div>
                </div><!-- /.col -->
                
                <?php echo form_close(); ?>
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#jenis-form").validate({
            rules:{
              nama_jenis : "required"
            },
            messages:{
              nama_jenis : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>