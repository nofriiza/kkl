      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Timeline Jadwal</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo $this->libdb->get_all_acara()->num_rows() ?></h3>
                  <p>Banyak Acara</p>
                </div>
                <div class="icon">
                  <i class="ion ion-clipboard"></i>
                </div>
                <a href="<?php echo site_url('admin/acara') ?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?php echo $this->libdb->get_all_pengumpulan()->num_rows() ?><sup style="font-size: 20px"></sup></h3>
                  <p>Banyak Pengumpulan</p>
                </div>
                <div class="icon">
                  <i class="ion ion-folder"></i>
                </div>
                <a href="<?php echo site_url('admin/ngumpul') ?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $this->libdb->get_all_dosen()->num_rows() ?></h3>
                  <p>Dosen Aktif</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo site_url('admin/dosen') ?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $this->libdb->get_thajaran_aktif() ?></h3>
                  <p>Tahun Ajaran</p>
                </div>
                <div class="icon">
                  <i class="ion ion-clock"></i>
                </div>
                <a href="#" class="small-box-footer">Tahun Ajaran Aktif <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div><!-- /.row -->
          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <section class="col-lg-6 connectedSortable">

              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Jadwal Acara</h3>
                  <div class="box-tools pull-right">
                    <!-- <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul> -->
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                  <?php if($acara->num_rows() > 0){ ?>
                    <?php foreach ($acara->result() as $rows) { ?>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text"><?php echo word_limiter($rows->judul,8) ?></span>
                      <small class="label label-default"><i class="fa fa-clock-o"></i><?php echo $rows->mulai ?></small>
                      <div class="tools">
                        <a href="<?php echo site_url('admin/pesertaacara/detail/'.$rows->id_acara) ?>" title="Detail"><i class="fa fa-eye"></i></a>
                      </div>
                    </li>
                    <?php } ?>
                  <?php } else { ?>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Tidak ada jadwal acara</span>
                      <!-- <small class="label label-default"><i class="fa fa-clock-o"></i><?php echo $rows->mulai ?></small> -->
                      <!-- <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div> -->
                    </li>
                  <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </section><!-- /.Left col -->
            <section class="col-lg-6 connectedSortable">

              <!-- TO DO List -->
              <div class="box box-primary">
                <div class="box-header">
                  <i class="ion ion-clipboard"></i>
                  <h3 class="box-title">Jadwal Pengumpulan</h3>
                  <!-- <div class="box-tools pull-right">
                    <ul class="pagination pagination-sm inline">
                      <li><a href="#">&laquo;</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                  </div> -->
                </div><!-- /.box-header -->
                <div class="box-body">
                  <ul class="todo-list">
                  <?php if($ngumpul->num_rows() > 0){ ?>
                    <?php foreach ($ngumpul->result() as $rows) { ?>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <!-- todo text -->
                      <span class="text"><?php echo $rows->judul ?></span>
                      <!-- Emphasis label -->
                      <small class="label label-danger"><i class="fa fa-clock-o"></i><?php echo $rows->waktu ?></small>
                      <!-- General tools such as edit or delete-->
                      <div class="tools">
                        <a href="<?php echo site_url('admin/ngumpul/detail/'.$rows->id_ngumpul) ?>" title="Detail"><i class="fa fa-eye"></i></a>
                      </div>
                    </li>
                    <?php } ?>
                  <?php } else { ?>
                    <li>
                      <span class="handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                      <input type="checkbox" value="" name="">
                      <span class="text">Tidak ada jadwal pengumpulan</span>
                      <!-- <small class="label label-default"><i class="fa fa-clock-o"></i><?php echo $rows->mulai ?></small> -->
                      <!-- <div class="tools">
                        <i class="fa fa-edit"></i>
                        <i class="fa fa-trash-o"></i>
                      </div> -->
                    </li>
                  <?php } ?>
                  </ul>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </section><!-- /.Left col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->