<style type="text/css">
table {
  width: 100%;
  border-collapse: collapse;
}

table, th, td {
  padding: 5px;
  border: 1px solid black;
}
</style>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm">
      <h1>
        <?php echo $kepanitiaan->nama_kepanitiaan ?>
      </h1>

      Ketua : <?php echo $this->libdb->get_one_dosen($kepanitiaan->ketua)->nama ?><br />
      Tanggal SK : <?php echo $kepanitiaan->tgl_sk ?>
      <table class="tabel">
      <?php $total = 0; ?>
        <tr>
          <th>No</th>
          <th>Nama Panitia</th>
          <th>Nilai</th>
          <th>Keterangan</th>
        </tr>
        <?php 
        $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
        foreach ($detkepanitiaan->result() as $rows) { 
        ?>                    
        <tr>
          <td><?php echo $no ?></td>                      
          <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
          <td><?php echo $rows->nilai ?></td>
          <td><?php echo $rows->keterangan ?></td>
        </tr>
        <?php $no++; } ?>
      </table>

      Total Panitia : <?php echo $num_rows ?><br />
</page>