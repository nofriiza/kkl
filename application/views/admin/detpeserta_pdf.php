<style type="text/css">
table {
  width: 100%;
  border-collapse: collapse;
}

table, th, td {
  padding: 3px;
  border: 1px solid black;
}
</style>
<?php 
  $batas = new DateTime($acara->mulai);
  $batas->modify("+30 minutes");
  ?>
<page backtop="14mm" backbottom="14mm" backleft="10mm" backright="10mm">
<h3><?php echo $acara->judul ?> (<?php echo $acara->mulai ?> Sampai <?php echo ($acara->jam_diakhiri != '0000-00-00 00:00:00') ? $acara->jam_diakhiri : $acara->selesai ?>)</h3>

  <table>
    <tr style="padding: 5px">
      <th>No</th>
      <th>Nama Peserta</th>
      <th>Jam Datang</th>
      <th>Nilai</th>
      <th>Keterangan</th>
    </tr>
    <?php 
    $no = 1;
    /*$sekarang = new DateTime("now");
    $mulai = new DateTime($acara->mulai);
    $selesai = new DateTime($acara->selesai);*/
    foreach ($peserta->result() as $rows) { 
    ?>                    
    <tr>
      <td><?php echo $no ?></td>                      
      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>

      <!-- <td><?php echo $rows->jam_datang ?></td>
      <td><?php echo $rows->nilai ?></td>
      <td><?php echo $rows->keterangan ?></td> -->
      <td>
        <?php
          if(substr($rows->keterangan,0,4) == 'Izin'){
            echo "Peserta Izin";
          } else if($rows->keterangan == 'Tidak Hadir Tanpa Keterangan' || (date('Y-m-d H:i:s') < $batas && $rows->nilai == '0')){
            echo "Peserta tidak hadir";
          } elseif($rows->jam_datang != "0000-00-00 00:00:00"){
            echo $rows->jam_datang;
          } elseif(date("Y-m-d H:i:s") < $acara->mulai) { 
            echo "Acara Belum Dimulai"; 
          } elseif ($rows->jam_datang == "0000-00-00 00:00:00") { 
            echo "Peserta Belum Datang"; 
          } else { 
            echo $rows->jam_datang;
        } ?>
      </td>
      <td><?php echo $rows->nilai ?></td>
      <td><?php echo $rows->keterangan ?></td>
    </tr>
    <?php $no++; } ?>
  </table>
    <br />
    <br />
    <br />
    Pembuat Acara : <?php echo $this->libdb->get_one_dosen($acara->created_by)->nama ?><br />
    Catatan Notulen : <?php echo $acara->notulen ?>
</page>