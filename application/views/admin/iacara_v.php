      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Acara
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/acara') ?>">Acara</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Acara</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/acara/simpan', array('id' => 'acara-form')); ?>
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <input type="hidden" name="idacara" value="<?php echo @$acara->id ?>">
                  <div class="form-group">
                    <label>Judul Acara</label>
                    <input type="text" name="judul" id="judul" class="form-control" value="<?php echo @$acara->judul ?>">
                  </div>
                  <div class="form-group">
                    <label>Tahun Ajaran</label>
                    <input type="text" name="thajaran" id="thajaran" class="form-control" value="<?php echo @$acara->thajaran ?>">
                  </div>
                  <div class="form-group">
                    <label>Jenis Acara Acara</label>
                    <select class="form-control select2" id="id_jenis" name="id_jenis" style="width: 100%;">
                      <option value="">Pilih Jenis Acara</option>
                      <?php foreach ($jenis->result() as $rows) { ?>
                      <option <?php echo ($rows->id == @$acara->id_jenis) ? "selected" : '' ?> value="<?php echo $rows->id?>"><?php echo $rows->nama_jenis ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group" id="dor">
                    <div style="padding:0px" class="col-md-6">
                      Waktu Mulai
                    </div>
                    <div class="col-md-6">
                      Waktu Selesai
                    </div>
                    <div style="padding:0px" class="col-md-6">
                      <input type="text" name="mulai" class="form-control" id="waktu-mulai" value="<?php echo @$acara->mulai ?>">
                    </div>
                    <div style="padding-right: 0px" class="col-md-6">
                      <input type="text" name="selesai" class="form-control" id="waktu-selesai" value="<?php echo @$acara->selesai ?>">
                    </div>
                  </div>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <?php if($this->uri->segment(3) == 'add'){ ?>
                  <div class="form-group">
                    <div class="radio">
                      <label>
                        <input type="radio" name="pilih" value="pilih">
                        Pilih
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="pilih" id="optionsRadios3" value="semua">
                        Semua
                      </label>
                    </div>                    
                    <div class="radio dosen" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios1" value="dosen">
                        Dosen
                      </label>
                    </div>
                    <div class="radio pegawai" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios2" value="pegawai">
                        Pegawai
                      </label>
                    </div>
                    <div class="radio semua" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios2" value="semua">
                        Semua
                      </label>
                    </div>
                  </div>
                  <!-- <div class="form-group" id="dosen">
                    <label>Peserta Acara</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Dosen" style="width: 100%;">
                      <?php foreach ($dosen->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group" id="pegawai">
                    <label>Peserta Acara</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Pegawai" style="width: 100%;">
                      <?php foreach ($pegawai->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div> -->
                  <div class="form-group" id="semua">
                    <label>Peserta Acara</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Peserta Acara" style="width: 100%;">
                      <?php foreach ($semua->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div><!-- /.form-group -->
                  <?php } ?>
                  <div class="form-group">
                    <label>Tempat Acara</label>
                    <input type="text" name="tempat" id="tempat" class="form-control" value="<?php echo @$acara->tempat ?>">
                  </div>
                  <div class="form-group">
                    <label>Keterangan Acara</label>
                    <textarea name="keterangan" id="keterangan" class="form-control"><?php echo @$acara->keterangan ?></textarea>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#acara-form").validate({
            rules:{
              judul : "required",
              id_jenis : "required",
              mulai : "required",
              selesai : "required",
              tempat : "required",
              "pilih" : "required",
              keterangan : "required"
            },
            messages:{
              judul : "<p class='text-red'>Field tidak boleh kosong</p>",
              id_jenis : "<p class='text-red'>Field tidak boleh kosong</p>",
              mulai : "<p class='text-red'>Field tidak boleh kosong</p>",
              selesai : "<p class='text-red'>Field tidak boleh kosong</p>",
              tempat : "<p class='text-red'>Field tidak boleh kosong</p>",
              "pilih" : "<p class='text-red'>Pilih Salah satu</p>",
              keterangan : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          });

          $(".dosen").hide();
          $(".semua").hide();
          $(".pegawai").hide();
          /*$("#dosen").hide();
          $("#pegawai").hide();*/
          $("#semua").hide();

          /*$('input[type=radio][name=option]').change(function() {
              if (this.value == 'dosen') {
                  $("#dosen").show("slow");
                  $("#pegawai").hide("slow");
                  $("#semua").hide("slow");
              }
              else if (this.value == 'pegawai') {
                  $("#dosen").hide("slow");
                  $("#pegawai").show("slow");
                  $("#semua").hide("slow");
              }
              else if (this.value == 'semua') {
                  $("#dosen").hide("slow");
                  $("#pegawai").hide("slow");
                  $("#semua").show("slow");
              }
          });*/

          $('input[type=radio][name=pilih]').change(function() {
              if (this.value == 'semua') {
                  $(".dosen").show("slow");
                  $(".pegawai").show("slow");
                  $(".semua").show("slow");
                  $("#semua").hide("slow");
                  /*if($('input[type=radio][name=option]').value == 'dosen'){
                    $("#dosen").show("slow");
                    $("#pegawai").hide("slow");
                    $("#semua").hide("slow");
                  } else if($('input[type=radio][name=option]').value == 'pegawai') {
                    $("#dosen").hide("slow");
                    $("#pegawai").show("slow");
                    $("#pegawai").hide("slow");
                  } else if($('input[type=radio][name=option]').value == 'semua') {
                    $("#dosen").hide("slow");
                    $("#pegawai").hide("slow");
                    $("#pegawai").show("slow");
                  } else {
                    $("#dosen").hide("slow");
                    $("#pegawai").hide("slow");
                    $("#pegawai").hide("slow");
                  }*/

              }
              else if (this.value == 'pilih') {
                  $(".dosen").hide("slow");
                  $(".pegawai").hide("slow");
                  $(".semua").hide("slow");
                  $("#semua").show("slow");
              }
          });

        });
      </script>