      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Rules
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/rules') ?>">Rules</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Rules</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <?php echo form_open('admin/rules/tambah', array('id' => 'rules-form')); ?>
                  <input type="hidden" name="idrules" value="<?php echo @$rules->id ?>">
                  <input type="hidden" name="jenis" id="jenis">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Jenis</label>
                    <select class="form-control select2" name="id_jenis" id="id_jenis" style="width: 100%;"  <?php echo ($this->session->userdata('rules_idjenis'))  ? "disabled='True'" : '' ?>>
                      <option value="">Pilih Jenis</option>
                      <?php foreach ($jenis->result() as $rows) { ?>
                      <option value="<?php echo $rows->id ?>" <?php echo (@$rows->id == $this->session->userdata('rules_idjenis')) ? "selected='selected'" : '' ?>><?php  echo $rows->nama_jenis?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group" id="dor">
                    <div style="padding:0px" class="col-md-8">
                      Waktu Terlambat
                    </div>
                    <div class="col-md-4">
                      Nilai
                    </div>
                    <div style="padding:0px" class="col-md-8 bootstrap-timepicker">
                      <input type="text" name="waktu" class="form-control timepicker">
                    </div>
                    <div style="padding-right: 0px" class="col-md-4">
                      <input type="text" name="nilai" class="form-control">
                    </div>
                  </div>
                  <div class="box-footer" style="margin-top:100px">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                  </div>
                  <?php echo form_close(); ?>
                </div><!-- /.col -->
                <div class="col-md-6">                  
                  <div class="col-xs-12">
                    <div class="box-header">
                      <h3 class="box-title">Rules</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                      <table class="table table-hover">
                        <tr>
                          <th>No</th>
                          <th>Waktu</th>
                          <th>Nilai</th>
                          <th>Action</th>
                        </tr>
                        <?php 
                          $no = 1; foreach ($this->cart->contents() as $rows) { 
                        ?>  
                        <tr>
                          <td><?php echo $no++ ?></td>
                          <td><?php echo $rows['option']['waktu'] ?></td>
                          <td><?php echo $rows['id'] ?></td>
                          <td><a title="hapus" onclick="return cek()" href="<?php echo site_url("admin/rules/hapuscart/".$rows['rowid']) ?>"><i class="fa fa-fw fa-close"></i></a><td>
                        </tr>
                        <?php } ?>
                      </table>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                      <a onclick="return cek_rules()" href="<?php echo site_url('admin/rules/simpan') ?>" class="btn btn-primary">Simpan</a>
                    </div>
                  </div>
                  
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Hapus Rule ?") == true){
            return true;
          } else {
            return false;
          }
        }

        function cek_rules() {
          if(confirm("Yakin Rule Sudah Benar ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>
      <script type="text/javascript">
        $(document).ready(function() {
          var idjenis = $("#id_jenis").val();
          if(!idjenis){
            $("#dor").hide();
          }

          $("#id_jenis").change(function(event) {
            $("#dor").show();
            $("#jenis").val($("#id_jenis").val());
          });

          $("#rules-form").validate({
            rules:{
              id_jenis : "required",
              operator : "required",
              waktu : "required",
              nilai : "required"
            },
            messages:{
              id_jenis : "<p class='text-red'>Field tidak boleh kosong</p>",
              operator : "<p class='text-red'>Field tidak boleh kosong</p>",
              waktu : "<p class='text-red'>Field tidak boleh kosong</p>",
              nilai : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>