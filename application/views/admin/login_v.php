<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url()?>asset/admin/login/css/style.css">    
  </head>
  <body>
    <hgroup>
  <h1>LOGIN ADMINISTRATOR</h1>
  <h3>STMIK EL-RAHMA YOGYAKARTA</h3>
  <?php if($this->session->flashdata('test')){?>
  <h5 style="color:red"><?php echo $this->session->flashdata('test') ?></h5>
  <?php } ?>
</hgroup>
<?php echo form_open('admin/login/login'); ?>
  <div class="group">
    <input type="text" name="username"><span class="highlight"></span><span class="bar"></span>
    <label>Username</label>
  </div>
  <div class="group">
    <input type="password" name="password"><span class="highlight"></span><span class="bar"></span>
    <label>Password</label>
  </div>
  <button type="submit" class="button buttonBlue">Login
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>
<footer>
  <p>Version <?php echo $this->config->item('version') ?></p>
</footer>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="<?php echo base_url()?>asset/admin/login/js/index.js"></script>
  </body>
</html>
