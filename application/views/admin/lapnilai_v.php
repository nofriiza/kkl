      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Laporan Nilai
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Laporan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h4 class="box-title">
                  <?php echo form_open('admin/laporan/nilai'); ?>
                  Tahun Ajaran
                  <select name="thajaran" onchange="this.form.submit()">
                    <?php $thajaranaktif = ($this->session->userdata('thajaran_lapoan')) ? $this->session->userdata('thajaran_lapoan') : 'semua' ?>
                    <option value="semua" <?php echo ($thajaranaktif == 'semua') ? 'selected' : ''; ?>>Semua</option>
                    <?php foreach ($thajaran->result() as $rows) { ?>
                      <option value="<?php echo $rows->thajaran ?>" <?php echo ($thajaranaktif == $rows->thajaran) ? 'selected' : ''; ?>><?php echo $rows->thajaran ?></option>
                    <?php } ?>
                  </select>
                  </form>
                  </h4>
                  <!-- <div class="box-tools">
                    <?php echo form_open('admin/rules/cari'); ?>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="cari_rules" class="form-control input-sm pull-right" value="<?php echo $this->session->userdata('cari_rules'); ?>">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                    <?php echo form_close(); ?>
                  </div> -->
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Nilai Acara (diikuti)</th>
                      <th>Nilai Laporan (diikuti)</th>
                      <th>Nilai Kepanitiaan (diikuti)</th>
                      <th>Rata-rata</th>
                      <th>Action</th>
                    </tr>
                    <?php $no = 1;foreach ($pegawai->result() as $rows) { ?>                    
                    <tr>
                      <td><?php echo $no ?></td>
                      <td><?php echo $rows->nama ?></td>
                      <?php 
                        $acara = $this->libdb->get_nilai_acara($rows->npp);
                      ?><?php $jumacara = (@$acara->jumacara) ? $acara->jumacara : '0' ?>
                      <td><?php echo @number_format($acara->rata, 2, ',', '').'('.$jumacara.')' ?></td>
                      <?php 
                        $laporan = $this->libdb->get_nilai_laporan($rows->npp);
                      ?><?php $jumngumpul =  (@$laporan->jumngumpul) ? $laporan->jumngumpul : '0'; ?>
                      <td><?php echo @number_format($laporan->rata, 2, ',', '').'('.$jumngumpul.')' ?></td>
                      <?php 
                        $kepanitiaan = $this->libdb->get_nilai_kepanitiaan($rows->npp);
                      ?><?php $jumngumpul =  (@$kepanitiaan->jumkepanitiaan) ? $kepanitiaan->jumkepanitiaan : '0'; ?>
                      <td><?php echo @number_format($kepanitiaan->rata, 2, ',', '').'('.$jumngumpul.')' ?></td>
                      <td><?php echo number_format(((@$acara->rata + @$laporan->rata +@$kepanitiaan->rata) / 3),2,',','') ?></td>
                      <td><a href="<?php echo site_url('admin/laporan/detail/'.$rows->npp) ?>" class="btn btn-info">Detail</a></td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
                <!-- <div class="box-footer">
                  Total Rows : <?php echo $num_rows ?>
                </div> -->
                
                <?php //echo $pagination ?>
                <!-- <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div> -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>