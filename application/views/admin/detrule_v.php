      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detail Rules Pengumpulan
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Rules</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Jenis : <?php echo $this->libdb->get_one_jenis_pengumpulan($jenis->id_jenis)->nama_jenis ?></h3>
                  <div class="box-tools">
                    <?php echo form_open('admin/rule/cari'); ?>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="cari_rule" class="form-control input-sm pull-right" value="<?php echo $this->session->userdata('cari_rule'); ?>">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                    <?php echo form_close(); ?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Waktu Terlambat</th>
                      <th>Nilai</th>
                      <th>Action</th>
                    </tr>
                    <?php $no = 1;foreach ($rule->result() as $rows) { ?>                    
                    <tr>
                      <td><?php echo $no ?></td>
                      <td><?php echo $rows->waktu. " Hari" ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td>
                        <a class="btn btn-success" href="<?php echo site_url('admin/rule/edit/'.$rows->id) ?>">Edit</a>
                        <a class="btn btn-danger" onclick="return cek()" href="<?php echo site_url('admin/rule/hapus/'.$rows->id) ?>">Hapus</a>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div>
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>