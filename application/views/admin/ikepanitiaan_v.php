      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Kepanitiaan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/kepanitiaan') ?>">Kepanitiaan</a></li>
            <li><a href="#"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?></a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Kepanitiaan</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/kepanitiaan/simpan', array('id' => 'kepanitiaan-form')); ?>
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <input type="hidden" name="idkepanitiaan" value="<?php echo @$kepanitiaan->id ?>">
                  <div class="form-group">
                    <label>Nama Kepanitiaan</label>
                    <input type="text" name="nama_kepanitiaan" id="nama_kepanitiaan" class="form-control" value="<?php echo @$kepanitiaan->nama_kepanitiaan ?>">
                  </div>
                  <div class="form-group">
                    <label>Tahun Ajaran</label>
                    <select class="form-control" name="thajaran">
                      <?php foreach ($thajaran->result() as $rows) { ?>
                        <option value="<?php echo $rows->thajaran ?>"><?php echo $rows->thajaran ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Tanggal SK</label>
                    <input type="text" name="tgl_sk" id="datepicker" class="form-control" value="<?php echo @tgl_datepicker($kepanitiaan->tgl_sk) ?>">
                  </div>
                  <div class="form-group">
                    <label>Ketua Kepanitiaan</label>
                    <select class="form-control select2" name="ketua" data-placeholder="Pilih Peserta Kepanitiaan" style="width: 100%;">
                      <?php foreach ($semua->result() as $rows) { ?>
                        <?php if($this->uri->segment(3) == 'edit'){ ?>
                        <option <?php echo (@$kepanitiaan->ketua == $rows->npp) ? 'selected' : 'disabled="disabled"'; ?> value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                        <?php } else { ?>
                        <option <?php echo (@$kepanitiaan->ketua == $rows->npp) ? 'selected' : ''; ?> value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                        <?php } ?>
                      <?php } ?>
                    </select>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#kepanitiaan-form").validate({
            rules:{
              nama_kepanitiaan : "required",
              thajaran : "required",
              tgl_sk : "required",
              ketua : "required"
            },
            messages:{
              nama_kepanitiaan : "<p class='text-red'>Field tidak boleh kosong</p>",
              thajaran : "<p class='text-red'>Field tidak boleh kosong</p>",
              tgl_sk : "<p class='text-red'>Field tidak boleh kosong</p>",
              ketua : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          });
      </script>