      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            List Kepanitiaan 
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Kepanitiaan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title"><a class="btn btn-primary" href="<?php echo site_url('admin/kepanitiaan/add') ?>">Tambah Data</a></h3>
                  <div class="box-tools">
                    <?php echo form_open('admin/kepanitiaan/cari'); ?>
                    <div class="input-group" style="width: 150px;">
                      <input type="text" name="cari_kepanitiaan" class="form-control input-sm pull-right" value="<?php echo $this->session->userdata('cari_kepanitiaan'); ?>">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" type="submit"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                    <?php echo form_close(); ?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Nama Kepanitiaan</th>
                      <th>Tahun Ajaran</th>
                      <th>Tanggal SK</th>
                      <th>Ketua</th>
                      <th>Action</th>
                    </tr>
                    <?php $no = 1;foreach ($kepanitiaan->result() as $rows) { ?>                    
                    <tr>
                      <td><?php echo $no ?></td>
                      <td><?php echo $rows->nama_kepanitiaan ?></td>
                      <td><?php echo $rows->thajaran?></td>
                      <td><?php echo tgl_indo($rows->tgl_sk) ?></td>
                      <td><?php echo $this->libdb->get_one_dosen($rows->ketua)->nama ?></td>
                      <th><a style="margin-right: 5px" class="btn btn-success" href="<?php echo site_url('admin/kepanitiaan/edit/'.$rows->id) ?>"> Edit</a><a style="margin-right: 5px" class="btn btn-info" href="<?php echo site_url('admin/kepanitiaan/detail/'.$rows->id) ?>">Detail</a><a style="margin-right: 5px" class="btn btn-danger" onclick="return cek()" href="<?php echo site_url('admin/kepanitiaan/delete/'.$rows->id)?>">Delete</a></th>
                    </tr>
                    <?php $no++; } ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  Total Rows : <?php echo $num_rows ?>
                </div>
                
                <?php echo $pagination ?>
                <!-- <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div> -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>