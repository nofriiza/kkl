      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tambah Data Pengumpulan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/ngumpul') ?>">Pengumpulan</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Data Pengumpulan</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/ngumpul/simpan', array('id' => 'ngumpul-form')); ?>
            <div class="box-body">
              <div class="row">

                <div class="col-md-6">
                  <input type="hidden" name="idngumpul" value="<?php echo @$ngumpul->id ?>">
                  
                  <input type="hidden" name="id_jenis" value="<?php echo @$ngumpul->id_jenis ?>">
                  <div class="form-group">
                    <label>Judul</label>
                    <input type="text" name="judul" class="form-control" value="<?php echo @$ngumpul->judul ?>">
                  </div>
                  <?php if($this->uri->segment(3) == 'edit'){ ?>
                  <div class="form-group">
                    <label>Jenis</label>
                    <input type="text" readonly class="form-control" value="<?php echo @$this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->nama_jenis ?>">
                  </div>                  
                  <?php } else { ?>
                  <div class="form-group">
                    <label>Jenis</label>
                    <select class="form-control select2" id="id_jenis" name="id_jenis" style="width: 100%;">
                      <option value="">Pilih Jenis</option>
                      <?php foreach ($jenis->result() as $rows) { ?>
                      <option <?php echo ($rows->id == @$ngumpul->id_jenis) ? "selected" : '' ?> id="<?php echo $rows->mk ?>" value="<?php echo $rows->id?>"><?php echo $rows->nama_jenis ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <?php } ?>
                </div><!-- /.col -->

                <div class="col-md-6">
                  <div class="form-group">
                    <label>Deadline Pengumpulan</label>
                    <input type="text" name="waktu" id="datepicker" class="form-control" value="<?php echo @tgl_datepicker($ngumpul->waktu) ?>">
                  </div>
                  <?php if($this->uri->segment(3) == 'add'){ ?>
                  <div class="form-group a48">
                    <div class="radio">
                      <label>
                        <input type="radio" name="pilih" value="pilih">
                        Pilih
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="pilih" value="semua">
                        Semua
                      </label>
                    </div>                    
                    <div class="radio dosen" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios1" value="dosen">
                        Dosen
                      </label>
                    </div>
                    <div class="radio pegawai" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios2" value="pegawai">
                        Pegawai
                      </label>
                    </div>
                    <div class="radio semua" style="padding-left: 20px">
                      <label>
                        <input type="radio" name="option" id="optionsRadios2" value="semua">
                        Semua
                      </label>
                    </div>
                  </div>
                  <?php /*<div class="form-group" id="dosen">
                    <label>Peserta Pengumpulan</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Dosen" style="width: 100%;">
                      <?php foreach ($dosen->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div><!-- /.form-group -->
                  <div class="form-group" id="pegawai">
                    <label>Peserta Pengumpulan</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Pegawai" style="width: 100%;">
                      <?php foreach ($pegawai->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div><!-- /.form-group -->*/?>
                  <div class="form-group a48" id="semua">
                    <label>Peserta Pengumpulan</label>
                    <select class="form-control select2" name="npp[]" multiple="multiple" data-placeholder="Pilih Peserta Pengumpulan" style="width: 100%;">
                      <?php foreach ($semua->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option> 
                      <?php } ?>
                    </select>
                  </div><!-- /.form-group -->
                  <?php } ?>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#ngumpul-form").validate({
            rules:{
              id_jenis : "required",
              judul : "required",
              "npp[]" : "required",
              waktu : "required"
            },
            messages:{
              id_jenis : "<p class='text-red'>Field tidak boleh kosong</p>",
              judul : "<p class='text-red'>Field tidak boleh kosong</p>",
              "npp[]" : "<p class='text-red'>Field tidak boleh kosong</p>",
              waktu : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          });

          $('.a48').hide()
          $(".dosen").hide();
          $(".semua").hide();
          $(".pegawai").hide();
          $("#semua").hide();

          $("#id_jenis").change(function() {
            if($(this).children(":selected").attr("id") == 'tidak'){
              $(".a48").show("slow");
              $('input[type=radio][name=pilih]').val() = 'pilih';
              $(".dosen").hide("slow");
              $(".semua").hide("slow");
              $(".pegawai").hide("slow");
              $("#semua").hide("slow");
            } else {
              $(".a48").hide("slow");
              $('input[type=radio][name=pilih]').val() = 'pilih';
              $(".dosen").hide("slow");
              $(".semua").hide("slow");
              $(".pegawai").hide("slow");
              $("#semua").hide("slow");
            }
          });

          $('input[type=radio][name=pilih]').change(function() {
              if (this.value == 'semua') {
                  $(".dosen").show("slow");
                  $(".pegawai").show("slow");
                  $(".semua").show("slow");
                  $("#semua").hide("slow");
                  /*if($('input[type=radio][name=option]').value == 'dosen'){
                    $("#dosen").show("slow");
                    $("#pegawai").hide("slow");
                    $("#semua").hide("slow");
                  } else if($('input[type=radio][name=option]').value == 'pegawai') {
                    $("#dosen").hide("slow");
                    $("#pegawai").show("slow");
                    $("#pegawai").hide("slow");
                  } else if($('input[type=radio][name=option]').value == 'semua') {
                    $("#dosen").hide("slow");
                    $("#pegawai").hide("slow");
                    $("#pegawai").show("slow");
                  } else {
                    $("#dosen").hide("slow");
                    $("#pegawai").hide("slow");
                    $("#pegawai").hide("slow");
                  }*/

              }
              else if (this.value == 'pilih') {
                  $(".dosen").hide("slow");
                  $(".pegawai").hide("slow");
                  $(".semua").hide("slow");
                  $("#semua").show("slow");
              }
          });

        });
      </script>