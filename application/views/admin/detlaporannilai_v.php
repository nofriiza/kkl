      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Detail Laporan <?php echo $dosen->nama." (".$dosen->npp.")" ?>
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Laporan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header" style="margin-bottom: 10px">
                  <h3 style="text-align: center">Acara</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                  <?php 
                  $totalnilai = 0; 
                  $noacara = 0;
                  ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Pengumpulan</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                    </tr>
                    <?php 
                    foreach ($acara->result() as $rows) { 
                      $noacara++;
                    ?>                    
                    <tr>
                      <td><?php echo $noacara ?></td>                      
                      <td><?php echo $rows->judul ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                    </tr>
                    <?php  $totalnilai = $totalnilai + $rows->nilai; } ?>
                    <?php if($noacara == 0){ ?> 
                    <tr>
                      <td colspan="4">Tidak ada acara yg di ikuti</td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                      <td colspan="2">Total & Rata-rata</td>
                      <td><?php echo $totalnilai ?></td>
                      <td><?php echo number_format($totalnilai / $noacara, 2, ',', '') ?></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header" style="margin-bottom: 10px">
                  <h3 style="text-align: center">Pengumpulan</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                  <?php 
                  $totallaporan = 0; 
                  $nolaporan = 0;
                  ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Acara</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                    </tr>
                    <?php 
                    foreach ($laporan->result() as $rows) { 
                      $nolaporan++;
                    ?>                    
                    <tr>
                      <td><?php echo $nolaporan ?></td>                      
                      <td><?php echo $rows->judul ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                    </tr>
                    <?php $totallaporan = $totallaporan + $rows->nilai; } ?>
                    <?php if($nolaporan == 0){ ?> 
                    <tr>
                      <td colspan="4">Tidak mengikuti pengumpulan</td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                      <td colspan="2">Total & Rata-rata</td>
                      <td><?php echo $totallaporan ?></td>
                      <td><?php echo number_format($totallaporan / $nolaporan, 2, ',', '') ?></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header" style="margin-bottom: 10px">
                  <h3 style="text-align: center">Kepanitiaan</h3>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                  <?php 
                  $totalkepanitiaan = 0; 
                  $nokepanitiaan = 0;
                  ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Acara</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                    </tr>
                    <?php 
                    foreach ($kepanitiaan->result() as $rows) { 
                      $nokepanitiaan++;
                    ?>                    
                    <tr>
                      <td><?php echo $nokepanitiaan ?></td>                      
                      <td><?php echo $rows->nama_kepanitiaan ?></td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                    </tr>
                    <?php $totalkepanitiaan = $totalkepanitiaan + $rows->nilai; } ?>
                    <?php if($nokepanitiaan == 0){ ?> 
                    <tr>
                      <td colspan="4">Tidak mengikuti pengumpulan</td>
                    </tr>
                    <?php } else { ?>
                    <tr>
                      <td colspan="2">Total & Rata-rata</td>
                      <td><?php echo $totalkepanitiaan ?></td>
                      <td><?php echo number_format($totalkepanitiaan / $nokepanitiaan, 2, ',', '') ?></td>
                    </tr>
                    <?php } ?>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>