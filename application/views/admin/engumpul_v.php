      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $ngumpul->judul ?>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/ngumpul') ?>">Ngumpul</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Peserta </h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/ngumpul/save', array('id' => 'ngumpul-form')); ?>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <input type="hidden" name="id_ngumpul" value="<?php echo @$ngumpul->id ?>">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <select name="npp" class="form-control select2">
                      <?php foreach ($dosen->result() as $rows) { ?>
                        <option value="<?php echo $rows->npp ?>"><?php echo $rows->nama ?></option>
                      <?php } ?>
                    </select>
                  </div>                  
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#ngumpul-form").validate({
            ngumpul:{
              npp : "required"
            },
            messages:{
              npp : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>