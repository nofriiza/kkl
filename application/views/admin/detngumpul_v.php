      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Judul : <?php echo $ngumpul->judul ?>
            <!-- <small>preview of simple tables</small> -->
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin/main') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <!-- <li><a href="#">Tables</a></li> -->
            <li class="active">Ngumpul</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <?php if($this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->mk == 'ya'){ ?>
                  <?php echo "Jenis : ".$this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->nama_jenis ?>
                  <?php } else { ?>
                  <?php if(date("Y-m-d") >= $ngumpul){ ?>
                  <h3 class="box-title"><a class="btn btn-primary" href="<?php echo site_url('admin/ngumpul/adddetail/'.$ngumpul->id) ?>">Tambah Peserta</a></h3>
                  <?php } else { ?>
                  <?php echo "Jenis : ".$this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->nama_jenis ?>
                  <?php } ?>
                  <?php } ?>
                  <div class="box-tools" style="float:right;padding-bottom: 20px">
                    <!-- <a href="LinkPrint / Bisa dihilangkan aja" class="btn btn-info" title="Print"><i class="fa fa-print"></i></a> -->
                    <a href="<?php echo site_url('admin/ngumpul/detail_excel/'.$ngumpul->id) ?>" class="btn btn-success" title="Download Excel"><i class="fa fa-file-excel-o"></i></a>
                    <a href="<?php echo site_url('admin/ngumpul/detail_pdf/'.$ngumpul->id)?>" target="_blank" class="btn btn-danger" title="Preview / Download PDF"><i class="fa fa-file-pdf-o"></i></a>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <?php if($this->session->flashdata('type') && $this->session->flashdata('pesan')){ ?>
                  <div class="alert alert-<?php echo $this->session->flashdata('type'); ?> alert-dismissable" style="margin: 20px">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4><i class="icon fa fa-<?php echo ($this->session->flashdata('type') == 'success') ? 'check' : 'info'  ?>"></i> <?php echo ($this->session->userdata('type') == 'success') ? "Disimpan" : "Dihapus" ?></h4>
                    <?php echo $this->session->flashdata('pesan'); ?>
                  </div>
                  <?php } ?>
                  <table class="table table-hover">
                  <?php $total = 0; if($this->libdb->get_one_jenis_pengumpulan($ngumpul->id_jenis)->mk == 'ya'){ ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Dosen</th>
                      <th>Tgl Dikumpul</th>
                      <th>Nama Matakuliah</th>
                      <th>Nilai</th>
                      <th>Action</th>
                    </tr>
                    <?php 
                    $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
                    foreach ($mk->result() as $rows) { 
                      $detngumpul = $this->ngumpul_m->get_one_data($ngumpul->id,$rows->npp,$rows->kodemk);
                      if(@$detngumpul->nilai){
                        $total++;
                      }
                    ?>                    
                    <tr>
                      <td><?php echo $no ?></td>                      
                      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
                      <td><?php
                        if (!@$detngumpul->tgl_dikumpul) { 
                          echo "Peserta Belum Mengumpulkan"; 
                        } else { 
                          echo $detngumpul->tgl_dikumpul;
                        } ?>
                      </td>
                      <td><?php $mk = $this->libdb->get_mk($rows->kodemk); echo $mk->namamk . " (" . $mk->kodemk . ")" ?></td>
                      <td><?php echo @$detngumpul->nilai ?></td>
                      <td>
                        <?php if(@!$detngumpul->tgl_dikumpul){ ?>
                        <a class="btn btn-info" onclick="return cekabsen()" href="<?php echo site_url('admin/ngumpul/soalnilai/'.$this->uri->segment(4)."/".$rows->npp)."/".$rows->kodemk ?>">Tandai Mengumpulkan</a>
                        <?php } else { ?>
                          Peserta Sudah Mengumpulkan
                        <?php } ?>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                  <?php } else { ?>
                    <tr>
                      <th>No</th>
                      <th>Nama Dosen</th>
                      <th>Tgl Dikumpul</th>
                      <th>Nilai</th>
                      <th>Keterangan</th>
                      <th>Action</th>
                    </tr>
                    <?php 
                    $no = $this->uri->segment(5) ? $this->uri->segment(5)+1 : 1;
                    foreach ($detngumpul->result() as $rows) { 
                      if($rows->nilai){
                        $total++;
                      }
                    ?>                    
                    <tr>
                      <td><?php echo $no ?></td>                      
                      <td><?php echo $this->libdb->get_one_dosen($rows->npp)->nama ?></td>
                      <td><?php
                        if (@$rows->tgl_dikumpul == '0000-00-00') { 
                          echo "Peserta Belum Mengumpulkan"; 
                        } else { 
                          echo $rows->tgl_dikumpul;
                        } ?>
                      </td>
                      <td><?php echo $rows->nilai ?></td>
                      <td><?php echo $rows->keterangan ?></td>
                      <td>
                        <?php if(!$rows->nilai){ ?>
                        <a class="btn btn-info" href="<?php echo site_url('admin/ngumpul/detngumpul/'.$rows->id) ?>">Mengumpulkan</a>
                        <?php } ?>
                        <?php 
                          if(date("Y-m-d") <= $ngumpul->waktu && !$rows->nilai){ ?>
                        <a class="btn btn-danger" onclick="return cek()" href="<?php echo site_url('admin/pesertangumpul/hapus/'.$rows->id) ?>">Hapus</a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php $no++; } ?>
                    <?php } ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer">
                  Total Peserta : <?php echo $num_rows ?><br />
                  Total Sudah Mengumpulkan : <?php echo $total_ngumpul ?>
                </div>
                
                <?php echo $pagination ?>
              </div><!-- /.box -->
            </div>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        function cek() {
          if(confirm("Yakin ingin menghapus ?") == true){
            return true;
          } else {
            return false;
          }
        }
        function cekabsen() {
          if(confirm("Dosen Sudah Mengumpulkan ?") == true){
            return true;
          } else {
            return false;
          }
        }
      </script>