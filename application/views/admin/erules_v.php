      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Edit Rule
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/rules') ?>">Rule</a></li>
            <li><a href="#">Edit</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $this->uri->segment(3) == 'add' ? "Tambah" : "Edit" ?> Rule</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/rules/save', array('id' => 'rules-form')); ?>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <input type="hidden" name="iddetrule" value="<?php echo @$rules->id ?>">
                  <div class="form-group" id="dor">
                    <div style="padding:0px" class="col-md-8">
                      Waktu Terlambat
                    </div>
                    <div class="col-md-4">
                      Nilai
                    </div>
                    <div style="padding:0px" class="col-md-8 bootstrap-timepicker">
                      <input type="text" name="waktu" class="form-control timepicker" value="<?php echo $rules->waktu ?>">
                    </div>
                    <div style="padding-right: 0px" class="col-md-4">
                      <input type="text" name="nilai" class="form-control" value="<?php echo $rules->nilai ?>">
                    </div>
                  </div>                  
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#rules-form").validate({
            rules:{
              waktu : "required",
              nilai : "required"
            },
            messages:{
              waktu : "<p class='text-red'>Field tidak boleh kosong</p>",
              nilai : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>