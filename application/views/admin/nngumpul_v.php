      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Mengumpulkan Laporan
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo site_url('admin') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/ngumpul') ?>">Ngumpul</a></li>
            <li><a href="#">Tambah</a></li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- SELECT2 EXAMPLE -->
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title"> </h3>
              <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
              </div>
            </div><!-- /.box-header -->
            <?php echo form_open('admin/ngumpul/ngumpul', array('id' => 'ngumpul-form')); ?>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <input type="hidden" name="id_ngumpul" value="<?php echo @$ngumpul->id_ngumpul ?>">
                  <input type="hidden" name="id_detngumpul" value="<?php echo @$ngumpul->id ?>">
                  <input type="hidden" name="npp" value="<?php echo @$ngumpul->npp ?>">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="text" class="form-control" value="<?php echo $this->libdb->get_one_dosen($ngumpul->npp)->nama ?>" readonly>
                  </div>                  
                </div><!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Keterangan</label>
                    <textarea name="keterangan" class="form-control"></textarea>
                  </div>                  
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            <?php echo form_close(); ?>
            <!-- <div class="box-footer">
              Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about the plugin.
            </div> -->
          </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <script type="text/javascript">
        $(document).ready(function() {
          $("#ngumpul-form").validate({
            ngumpul:{
              waktu : "required",
              nilai : "required"
            },
            messages:{
              waktu : "<p class='text-red'>Field tidak boleh kosong</p>",
              nilai : "<p class='text-red'>Field tidak boleh kosong</p>"
            }
          })
        });
      </script>