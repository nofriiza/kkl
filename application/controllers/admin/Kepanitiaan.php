<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kepanitiaan extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/kepanitiaan_m');
    }

    function index($offset = '') {
    	
    	$this->session->unset_userdata('cari_kepanitiaan','');

    	$limit = 10;
    	$data['title'] = "List Kepanitiaan";
    	$data['main'] = "admin/tkepanitiaan_v";
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->kepanitiaan_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/kepanitiaan/index'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
    	if($this->input->post('cari_kepanitiaan')){
            $this->session->set_userdata('cari_kepanitiaan',$this->input->post('cari_kepanitiaan'));
        }

    	$limit = 10;
    	$data['title'] = "List Kepanitiaan";
    	$data['main'] = "admin/tkepanitiaan_v";
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->kepanitiaan_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/kepanitiaan/index'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function detail($id)
    {
    	$this->session->set_userdata('idkepanitiaan',$id);

    	$limit = 10;
    	$data['title'] = "List Peserta Kepanitiaan";
    	$data['main'] = "admin/detkepanitiaan_v";
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_one($id);
    	$data['detkepanitiaan'] = $this->kepanitiaan_m->get_all_detail($id);
    	$data['num_rows'] = $this->kepanitiaan_m->count_all_detail($id);
    	$data['pagination'] = pagination(site_url('admin/kepanitiaan/detail/'.$id),$data['num_rows'],$limit,5);
        $this->load->view('admin/main_v', $data);
    }

    public function editdetail($id)
    {
    	$data['title'] = "Edit Peserta Kepanitiaan";
    	$data['main'] = 'admin/idetkepanitiaan_v';
    	$data['detkepanitiaan'] = $this->kepanitiaan_m->get_one_detail($id);
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_one($data['detkepanitiaan']->id_kepanitiaan);
    	$data['peserta'] = $this->libdb->get_peserta_kepanitiaan($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function detail_pdf($id)
    {
    	$this->load->library('html2pdf/html2pdf');
        $this->session->unset_userdata('cari_kepanitiaan','');
        $data['title'] = "List Panitia";
        $data['kepanitiaan'] = $this->kepanitiaan_m->get_one($id);
        $data['detkepanitiaan'] = $this->kepanitiaan_m->get_all_detail($id);
        ob_start();
        $data['num_rows'] = $this->kepanitiaan_m->count_all_detail($id);
        $this->load->view('admin/detkepanitiaan_pdf', $data);
        $content = ob_get_clean();
        date_default_timezone_set('Asia/Jakarta');
        $filename = "List Peserta ".$data['kepanitiaan']->nama_kepanitiaan;
        $html2pdf = new HTML2PDF('P','A4','fr');
        $html2pdf->WriteHTML($content);
        $html2pdf->Output($filename.'.pdf');
    }

    public function detail_excel($id)
    {
        $data['title'] = "List Pekepanitiaanan";
        $data['kepanitiaan'] = $this->kepanitiaan_m->get_one($id);
        $data['detkepanitiaan'] = $this->kepanitiaan_m->get_all_detail($id);
        $data['num_rows'] = $this->kepanitiaan_m->count_all_detail($id);
        $this->load->view('admin/detkepanitiaan_excel', $data);
    }

    public function tambahdetail($id)
    {
    	$data['title'] = "Tambah Peserta Kepanitiaan";
    	$data['main'] = 'admin/idetkepanitiaan_v';
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_one($id);
    	$data['peserta'] = $this->libdb->get_peserta_kepanitiaan($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function hapusdetail($id)
    {
    	$this->kepanitiaan_m->hapusdetail($id);
    	$this->session->set_flashdata('type','info');
    	$this->session->set_flashdata('pesan','Data Berhasil dihapus');
    	redirect('admin/kepanitiaan/detail/'.$id);
    }

    public function simpandetail()
    {
    	$id = $this->input->post('idkepanitiaan');
    	$this->kepanitiaan_m->simpandetail();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/kepanitiaan/detail/'.$id);
    }

    public function add()
    {
    	$this->load->model('admin/peserta_m');
    	$data['title'] = "Tambah Kepanitiaan";
    	$data['main'] = "admin/ikepanitiaan_v";
    	$data['semua'] = $this->peserta_m->get_dosen_input();
    	$data['thajaran'] = $this->libdb->get_thajaran();
    	$this->load->view('admin/main_v', $data);
    }

    public function edit($id)
    {
    	$this->load->model('admin/peserta_m');
    	$data['title'] = "Tambah Kepanitiaan";
    	$data['main'] = "admin/ikepanitiaan_v";
    	$data['kepanitiaan'] = $this->kepanitiaan_m->get_one($id);
    	$data['semua'] = $this->peserta_m->get_dosen_input();
    	$data['thajaran'] = $this->libdb->get_thajaran();
    	$this->load->view('admin/main_v', $data);
    }

    public function delete($id)
    {
    	$this->kepanitiaan_m->delete($id);
    	$this->session->set_flashdata('type','info');
    	$this->session->set_flashdata('pesan','Data Berhasil dihapus');
    	redirect('admin/kepanitiaan');
    }

    public function simpan()
    {
    	$this->kepanitiaan_m->simpan();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/kepanitiaan');
    }
}
        