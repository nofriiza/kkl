<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function nilai() {
    	if($this->input->post('thajaran')){
    		if($this->input->post('thajaran') == 'semua'){    			
    			$this->session->set_userdata('thajaran_lapoan','');
    		} else {
    			$this->session->set_userdata('thajaran_lapoan',$this->input->post('thajaran'));
    		}
    	}
    	$data['title'] = "Laporan Nilai Dosen";
    	$data['pegawai'] = $this->libdb->get_all_dosen();
    	$data['thajaran'] = $this->libdb->get_thajaran();
    	$data['main'] = 'admin/lapnilai_v';
        $this->load->view('admin/main_v', $data);
    }

    function detail($npp)
    {
    	$data['title'] = "Detail Laporn Nilai Dosen";
    	$data['main'] = 'admin/detlaporannilai_v';
    	$data['dosen'] = $this->libdb->get_one_dosen($npp);
    	$data['laporan'] = $this->libdb->get_all_nilai_ngumpul($npp);
    	$data['acara'] = $this->libdb->get_all_nilai_acara($npp);
    	$data['kepanitiaan'] = $this->libdb->get_all_nilai_kepanitiaan($npp);
    	$this->load->view('admin/main_v', $data);
    }
}
        