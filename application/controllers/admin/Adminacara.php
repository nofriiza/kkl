<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminacara extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
    	$data['main'] = "admin/tadminacara_v";
    	$data['title'] = "Administrator Acara";
    	$data['adminacara'] = $this->libdb->get_all_admin_acara();
        $this->load->view('admin/main_v', $data);
    }

    public function add()
    {
    	$data['main'] = "admin/iadminacara_v";
    	$data['title'] = "Tambah Administrator Acara";
    	$data['dosen'] = $this->libdb->get_all_dosen();
        $this->load->view('admin/main_v', $data);
    }

    public function simpan()
    {
    	$this->libdb->simpan_admin_acara();
    	redirect('admin/adminacara');
    }

    public function hapus($id)
    {
    	$this->libdb->hapus_admin_acara($id);
    	redirect('admin/adminacara');
    }
}