<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenisngumpul extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/jenisngumpul_m');
    }

    function index($offset = '') 
    {
        $this->session->unset_userdata('cari_jenisngumpul','');
        
    	$limit = 10;
    	$data['title'] = "List Jenis Acara";
    	$data['main'] = "admin/tjenisngumpul_v";
    	$data['jenisngumpul'] = $this->jenisngumpul_m->get_all($limit,$offset);
        $data['num_rows'] = $this->jenisngumpul_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/jenisngumpul/index/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
    	if($this->input->post('cari_jenisngumpul')){
    		$this->session->set_userdata('cari_jenisngumpul',$this->input->post('cari_jenisngumpul'));
    	}

    	$limit = 10;
    	$data['title'] = "List Jenis Acara";
    	$data['main'] = "admin/tjenisngumpul_v";
    	$data['jenisngumpul'] = $this->jenisngumpul_m->get_all($limit,$offset);
        $data['num_rows'] = $this->jenisngumpul_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/jenisngumpul/cari/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function add()
    {
    	$data['title'] = "Tambah Jenis Acara";
    	$data['main'] = "admin/ijenisngumpul_v";
    	$this->load->view('admin/main_v', $data);
    }

    public function edit($id)
    {
    	$data['title'] = "Tambah Jenis Acara";
    	$data['main'] = "admin/ijenisngumpul_v";
    	$data['jenisngumpul'] = $this->jenisngumpul_m->get_one($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function hapus($id)
    {
    	$this->jenisngumpul_m->delete($id);
        $this->session->set_flashdata('type','info');
        $this->session->set_flashdata('pesan','Data Berhasil Dihapus');
    	redirect('admin/jenisngumpul');
    }

    public function simpan()
    {
		$this->jenisngumpul_m->simpan();
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/jenisngumpul');
    }
}