<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pesertaacara extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/peserta_m');
    }

    function detail($id) {
    	$this->session->set_userdata('idacara',$id);
    	$data['title'] = "List Peserta Acara";
    	$data['main'] = "admin/detpeserta_v";
    	$data['acara'] = $this->libdb->get_one_acara($id);
    	$data['peserta'] = $this->peserta_m->get_all($id);
        $this->load->view('admin/main_v', $data);
    }

    function detail_pdf($id) {
        $this->load->library('html2pdf/html2pdf');
        $this->session->set_userdata('idacara',$id);
        $data['title'] = "List Peserta Acara";
        $data['main'] = "admin/detpeserta_v";
        $data['acara'] = $this->libdb->get_one_acara($id);
        $data['peserta'] = $this->peserta_m->get_all($id);
        ob_start();
        $this->load->view('admin/detpeserta_pdf', $data);
        $content = ob_get_clean();
        date_default_timezone_set('Asia/Jakarta');
        $filename = "List Peserta ".$data['acara']->judul;
        $html2pdf = new HTML2PDF('P','A4','fr');
        $html2pdf->WriteHTML($content);
        $html2pdf->Output($filename.'.pdf');
    }

    public function detail_excel($id)
    {
        $this->load->library('html2pdf/html2pdf');
        $this->session->set_userdata('idacara',$id);
        $data['title'] = "List Peserta Acara";
        $data['main'] = "admin/detpeserta_v";
        $data['acara'] = $this->libdb->get_one_acara($id);
        $data['peserta'] = $this->peserta_m->get_all($id);
        $this->load->view('admin/detpeserta_excel', $data);
    }

    function add()
    {
    	$data['title'] = "Tambah Peserta Acara";
    	$data['main'] = "admin/ipeserta_v";
    	$data['dosen'] = $this->peserta_m->get_dosen_input($this->session->userdata('idacara'));
    	$data['acara'] = $this->libdb->get_one_acara($this->session->userdata('idacara'));
    	$this->load->view('admin/main_v',$data);
    }

    function edit($id)
    {
    	$data['title'] = "Edit Peserta";
    	$data['main'] = "admin/epeserta_v";
    	$data['peserta'] = $this->peserta_m->get_one($id);
    	$this->load->view('admin/main_v',$data);
    }

    function ijin($id)
    {
        $data['title'] = "Tambah Keterangan";
        $data['main'] = "admin/epeserta_v";
        $data['peserta'] = $this->peserta_m->get_one($id);
        $this->load->view('admin/main_v',$data);
    }

    function simpan()
    {
    	$this->peserta_m->simpan();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/pesertaacara/detail/'.$this->session->userdata('idacara'));
    }

    function selesai()
    {
        $this->peserta_m->selesai();
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Acara Sudah Selesai');
        redirect('admin/pesertaacara/detail/'.$this->session->userdata('idacara'));
    }

    function save()
    {
        if($this->input->post('edit')){
           $this->peserta_m->save('');
        } else {
           $this->peserta_m->save('','ya');
        }
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Keterangan Peserta Ditambah');
    	redirect('admin/pesertaacara/detail/'.$this->session->userdata('idacara'));
    }

    function absen($id)
    {
    	$this->peserta_m->save($id);
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Peserta Sudah Absen');
    	redirect('admin/pesertaacara/detail/'.$this->session->userdata('idacara'));
    }

    function hapus($id)
    {
    	$this->peserta_m->hapus($id);
    	$this->session->set_flashdata('type','info');
    	$this->session->set_flashdata('pesan','Data Berhasil Dihapus');
    	redirect('admin/pesertaacara/detail/'.$this->session->userdata('idacara'));
    }
}
        