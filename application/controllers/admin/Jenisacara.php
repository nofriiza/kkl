<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenisacara extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/jenisacara_m');
    }

    function index($offset = '') 
    {
        $this->session->unset_userdata('cari_jenisacara','');
        
    	$limit = 10;
    	$data['title'] = "List Jenis Acara";
    	$data['main'] = "admin/tjenisacara_v";
    	$data['jenisacara'] = $this->jenisacara_m->get_all($limit,$offset);
        $data['num_rows'] = $this->jenisacara_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/jenisacara/index/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
    	if($this->input->post('cari_jenisacara')){
    		$this->session->set_userdata('cari_jenisacara',$this->input->post('cari_jenisacara'));
    	}

    	$limit = 10;
    	$data['title'] = "List Jenis Acara";
    	$data['main'] = "admin/tjenisacara_v";
    	$data['jenisacara'] = $this->jenisacara_m->get_all($limit,$offset);
        $data['num_rows'] = $this->jenisacara_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/jenisacara/cari/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function add()
    {
    	$data['title'] = "Tambah Jenis Acara";
    	$data['main'] = "admin/ijenisacara_v";
    	$this->load->view('admin/main_v', $data);
    }

    public function edit($id)
    {
    	$data['title'] = "Tambah Jenis Acara";
    	$data['main'] = "admin/ijenisacara_v";
    	$data['jenisacara'] = $this->jenisacara_m->get_one($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function hapus($id)
    {
    	$this->jenisacara_m->delete($id);
        $this->session->set_flashdata('type','info');
        $this->session->set_flashdata('pesan','Data Berhasil Dihapus');
    	redirect('admin/jenisacara');
    }

    public function simpan()
    {
		$this->jenisacara_m->simpan();
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/jenisacara');
    }
}
        