<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rules extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/rules_m');
        $this->load->library('cart');
    }

    function index($offset = '') {
    	$limit = 10;
    	$data['title'] = "List Rules Acara";
    	$data['main'] = "admin/trules_v";
    	$data['rules'] = $this->rules_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->rules_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/rules/index/'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function detail($id = '')
    {
    	$limit = 10;
    	$data['title'] = "Detail Rules Acara";
    	$data['main'] = "admin/detrules_v";
        $data['rules'] = $this->rules_m->get_detail($id);
    	$data['jenis'] = $this->rules_m->get_one($id);
        $this->load->view('admin/main_v', $data);
    }

 	public function cari($offset = '')
    {
    	if($this->input->post('cari_rules')){
    		$this->session->set_userdata('cari_rules',$this->input->post('cari_rules'));
    	}

    	$limit = 2;
    	$data['title'] = "List Rules Acara";
    	$data['main'] = "admin/trules_v";
    	$data['rules'] = $this->rules_m->get_all($limit,$offset);
        $data['num_rows'] = $this->rules_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/rules/cari/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function add()
    {
    	if($this->cart->total_items() < 1){
    		$this->session->unset_userdata('rules_idjenis');
    	}
    	$data['title'] = "Tambah Rules Acara";
    	$data['main'] = "admin/irules_v";
    	$data['jenis'] = $this->libdb->get_jenis_acara();
    	$this->load->view('admin/main_v', $data);
    }

    public function tambah()
    {
    	if($this->input->post('jenis') != ''){
    		$this->session->set_userdata('rules_idjenis',$this->input->post('jenis'));
    	}

		$hehe = array(
			'id' 		=> $this->input->post('nilai'), 
			'qty' 		=> $this->session->userdata('rules_idjenis'),
			'price' 	=> 3,
			'name' 		=> 'a',
			'option' 	=> array('operator' => $this->input->post('operator'),'waktu' => $this->input->post('waktu'))
		);
		$this->cart->insert($hehe);

    	redirect('admin/rules/add');
    }

    public function hapuscart($id)
    {
    	$data = array(
			'rowid' => $id,
			'qty'	=> 0 
			);
		$this->cart->update($data);
		redirect('admin/rules/add');
    }

    public function edit($id)
    {
    	$data['title'] = "Tambah Rules Acara";
    	$data['main'] = "admin/erules_v";
    	$data['rules'] = $this->rules_m->get_one_detail($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function hapus($id)
    {
    	$this->rules_m->delete($id);
        $this->session->set_flashdata('type','info');
        $this->session->set_flashdata('pesan','Data Berhasil dihapus');
    	redirect('admin/rules');
    }

    public function simpan()
    {
		if($this->cart->total_items() > 0){
			$this->rules_m->simpan();
			$this->cart->destroy();
		}
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/rules');
    }

    public function save($value='')
    {
		$this->rules_m->save();
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/rules');
    }
}