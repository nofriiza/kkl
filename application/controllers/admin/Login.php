<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = "Login Administrator";
		$this->load->view('admin/login_v', $data);
	}

	public function login()
	{
		$this->load->model('admin/login_m');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		if($cek = $this->login_m->ceklogin($username,$password)){
			//tambah session disini
			$adminacara = $this->libdb->get_admin_acara($cek->npp);
			$status = ($adminacara->num_rows() > 0) ? "admin" : $cek->status ;
			$this->session->set_userdata('npp',$cek->npp);
			$this->session->set_userdata('status',$status);
			$this->session->set_userdata('is_login',true);

			redirect('admin/main');
		} else {
			$this->session->set_flashdata('test', 'ID Atau Password Anda Salah');
			redirect('admin/login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('npp');
		$this->session->unset_userdata('status');
		$this->session->unset_userdata('is_login');
		redirect('admin/login');
	}

}