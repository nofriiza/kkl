<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acara extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/acara_m');
    }

    function index($offset = '') {

    	$this->session->unset_userdata('cari_acara','');

    	$limit = 10;
    	$data['title'] = "List Acara";
    	$data['main'] = 'admin/tacara_v';
    	$data['acara'] = $this->acara_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->acara_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/acara/index/'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
        if($this->input->post('cari_acara')){
            $this->session->set_userdata('cari_acara',$this->input->post('cari_acara'));
        }

        $limit = 10;
        $data['title'] = "List Acara";
        $data['main'] = "admin/tacara_v";
        $data['acara'] = $this->acara_m->get_all($limit,$offset);
        $data['num_rows'] = $this->acara_m->count_all();

        $data['pagination'] = pagination(site_url('admin/acara/cari/'),$data['num_rows'],$limit,4);

        $this->load->view('admin/main_v', $data);
    }

    public function add()
    {
        $this->load->model('admin/peserta_m');
    	$data['title'] = "Tambah Acara";
    	$data['main'] = "admin/iacara_v";
        $data['semua'] = $this->peserta_m->get_dosen_input();
        $data['pegawai'] = $this->peserta_m->get_pegawai_input();
        $data['dosen'] = $this->peserta_m->get_dosens_input();
    	$data['acara'] = $this->libdb->get_acara();
        $data['jenis'] = $this->libdb->get_jenis_acara();
    	$this->load->view('admin/main_v', $data);
    }

    public function hapus($id)
    {
        $this->acara_m->delete($id);
        $this->session->set_flashdata('type','info');
        $this->session->set_flashdata('pesan','Data Berhasil Dihapus');
        redirect('admin/acara');
    }

    public function edit($id)
    {
    	$data['title'] = "Edit Acara";
    	$data['main'] = "admin/iacara_v";
    	//$data['acara'] = $this->libdb->get_acara();
        $data['jenis'] = $this->libdb->get_jenis_acara();
    	$data['acara'] = $this->acara_m->get_one($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function simpan()
    {
    	$this->acara_m->simpan();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/acara');
    }
}