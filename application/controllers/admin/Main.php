<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = "Dashboard Sistem";
		$data['main'] = "admin/dashboard_v";
		$data['acara'] = $this->libdb->get_jadwal_acara();
		$data['ngumpul'] = $this->libdb->get_jadwal_ngumpul();
		$this->load->view('admin/main_v', $data);
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */