<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rule extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/rule_m');
        $this->load->library('cart');
    }

    function index($offset = '') {
    	$limit = 10;
    	$data['title'] = "List Rules Pengumpuan";
    	$data['main'] = "admin/trule_v";
    	$data['rule'] = $this->rule_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->rule_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/rule/index/'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function detail($id = '')
    {
    	$limit = 10;
    	$data['title'] = "Detail Rule Pengumpulan";
    	$data['main'] = "admin/detrule_v";
        $data['rule'] = $this->rule_m->get_detail($id);
    	$data['jenis'] = $this->rule_m->get_one($id);
        $this->load->view('admin/main_v', $data);
    }

 	public function cari($offset = '')
    {
    	if($this->input->post('cari_rule')){
    		$this->session->set_userdata('cari_rule',$this->input->post('cari_rule'));
    	}

    	$limit = 2;
    	$data['title'] = "List Rule Pengumpulan";
    	$data['main'] = "admin/trule_v";
    	$data['rule'] = $this->rule_m->get_all($limit,$offset);
        $data['num_rows'] = $this->rule_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/rule/cari/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function add()
    {
    	if($this->cart->total_items() < 1){
    		$this->session->unset_userdata('rule_idjenis');
    	}
    	$data['title'] = "Tambah Rule Pengumpulan";
    	$data['main'] = "admin/irule_v";
    	$data['jenis'] = $this->libdb->get_jenis_pengumpulan();
    	$this->load->view('admin/main_v', $data);
    }

    public function tambah()
    {
    	if($this->input->post('jenis') != ''){
    		$this->session->set_userdata('rule_idjenis',$this->input->post('jenis'));
    	}

		$hehe = array(
			'id' 		=> $this->input->post('nilai'), 
			'qty' 		=> $this->session->userdata('rule_idjenis'),
			'price' 	=> 3,
			'name' 		=> 'a',
			'option' 	=> array('operator' => $this->input->post('operator'),'waktu' => $this->input->post('waktu'))
		);
		$this->cart->insert($hehe);

    	redirect('admin/rule/add');
    }

    public function hapuscart($id)
    {
    	$data = array(
			'rowid' => $id,
			'qty'	=> 0 
			);
		$this->cart->update($data);
		redirect('admin/rule/add');
    }

    public function edit($id)
    {
    	$data['title'] = "Edit Rule";
    	$data['main'] = "admin/erule_v";
    	$data['rule'] = $this->rule_m->get_one_detail($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function hapus($id)
    {
    	$this->rule_m->delete($id);
        $this->session->set_flashdata('type','info');
        $this->session->set_flashdata('pesan','Data Berhasil dihapus');
    	redirect('admin/rule');
    }

    public function simpan()
    {
		if($this->cart->total_items() > 0){
			$this->rule_m->simpan();
			$this->cart->destroy();
		}
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/rule');
    }

    public function save($value='')
    {
		$this->rule_m->save();
        $this->session->set_flashdata('type','success');
        $this->session->set_flashdata('pesan','Data Berhasil disimpan');
		redirect('admin/rule');
    }
}