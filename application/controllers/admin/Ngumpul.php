<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ngumpul extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/ngumpul_m');
    }

    function index($offset = '') {

    	$this->session->unset_userdata('cari_ngumpul','');

    	$limit = 10;
    	$data['title'] = "List Pengumpulan";
    	$data['main'] = 'admin/tngumpul_v';
    	$data['ngumpul'] = $this->ngumpul_m->get_all();
    	$data['num_rows'] = $this->ngumpul_m->count_all();
    	$data['pagination'] = pagination(site_url('admin/ngumpul/index/'),$data['num_rows'],$limit,4);
        $this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
        if($this->input->post('cari_ngumpul')){
            $this->session->set_userdata('cari_ngumpul',$this->input->post('cari_ngumpul'));
        }

        $limit = 10;
        $data['title'] = "List Pengumpulan";
        $data['main'] = "admin/tngumpul_v";
        $data['ngumpul'] = $this->ngumpul_m->get_all($limit,$offset);
        $data['num_rows'] = $this->ngumpul_m->count_all();

        $data['pagination'] = pagination(site_url('admin/ngumpul/cari/'),$data['num_rows'],$limit,4);

        $this->load->view('admin/main_v', $data);
    }

    public function add()
    {
        $this->load->model('admin/peserta_m');
    	$data['title'] = "Tambah Data Pengumpulan";
    	$data['main'] = "admin/ingumpul_v";
    	$data['jenis'] = $this->libdb->get_jenis_pengumpulan();
        $data['semua'] = $this->peserta_m->get_dosen_input();
        $data['pegawai'] = $this->peserta_m->get_pegawai_input();
        $data['dosen'] = $this->peserta_m->get_dosens_input();
    	$this->load->view('admin/main_v', $data);
    }

    public function adddetail($id)
    {
    	$data['title'] = "Tambah Data Pengumpulan";
    	$data['main'] = "admin/engumpul_v";
    	$data['ngumpul'] = $this->ngumpul_m->get_one($id);
    	$data['dosen'] = $this->ngumpul_m->get_dosen_tambah($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function edit($id)
    {
    	$data['title'] = "Edit Data Pengumpulan";
    	$data['main'] = "admin/ingumpul_v";
    	$data['ngumpul'] = $this->ngumpul_m->get_one($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function detngumpul($id)
    {
    	$data['title'] = "Data Pengumpulan";
    	$data['main'] = "admin/nngumpul_v";
    	$data['ngumpul'] = $this->ngumpul_m->get_one_detail($id);
    	$this->load->view('admin/main_v', $data);
    }

    public function ngumpul()
    {
    	$id = $this->ngumpul_m->ngumpul();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/ngumpul/detail/'.$id);
    }

    public function simpan()
    {
    	$this->ngumpul_m->simpan();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/ngumpul');
    }

    public function save()
    {
    	$id = $this->ngumpul_m->save();
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/ngumpul/detail/'.$id);
    }

    public function detail($id,$offset = 0)
    {
    	$this->session->unset_userdata('cari_ngumpul','');

    	$limit = 10;
    	$data['title'] = "List Pengumpulan";
    	$data['main'] = 'admin/detngumpul_v';
    	$data['ngumpul'] = $this->ngumpul_m->get_one($id);
    	$data['detngumpul'] = $this->ngumpul_m->get_all_detail($limit,$offset,$id);
    	if($this->libdb->get_one_jenis_pengumpulan($data['ngumpul']->id_jenis)->mk == 'ya'){
    		$data['mk'] = $this->libdb->get_all_mk($limit,$offset);
    		$data['num_rows'] = $this->libdb->count_all_dosen($id);
            $data['total_ngumpul'] = $this->libdb->count_all_ngumpul($id);
    	}else {
    		$data['num_rows'] = $this->ngumpul_m->count_all_detail($id);
            $data['total_ngumpul'] = $this->ngumpul_m->count_all_ngumpul($id);
    	}
    	$data['pagination'] = pagination(site_url('admin/ngumpul/detail/'.$id."/"),$data['num_rows'],$limit,5);
        $this->load->view('admin/main_v', $data);
    }

    public function detail_pdf($id)
    {
        $this->load->library('html2pdf/html2pdf');
        $this->session->unset_userdata('cari_ngumpul','');
        $data['title'] = "List Pengumpulan";
        $data['ngumpul'] = $this->ngumpul_m->get_one($id);
        $data['detngumpul'] = $this->ngumpul_m->get_all_detail('','',$id);
        if($this->libdb->get_one_jenis_pengumpulan($data['ngumpul']->id_jenis)->mk == 'ya'){
            $data['mk'] = $this->libdb->get_all_mk();
            $data['num_rows'] = $this->libdb->count_all_dosen($id);
            $data['total_ngumpul'] = $this->libdb->count_all_ngumpul($id);
        }else {
            $data['num_rows'] = $this->ngumpul_m->count_all_detail($id);
            $data['total_ngumpul'] = $this->ngumpul_m->count_all_ngumpul($id);
        }
        ob_start();
        $this->load->view('admin/detngumpul_pdf', $data);
        $content = ob_get_clean();
        date_default_timezone_set('Asia/Jakarta');
        $filename = "List Peserta ".$data['ngumpul']->judul;
        $html2pdf = new HTML2PDF('P','A4','fr');
        $html2pdf->WriteHTML($content);
        $html2pdf->Output($filename.'.pdf');
    }

    public function detail_excel($id)
    {
        $data['title'] = "List Pengumpulan";
        $data['ngumpul'] = $this->ngumpul_m->get_one($id);
        $data['detngumpul'] = $this->ngumpul_m->get_all_detail('','',$id);
        if($this->libdb->get_one_jenis_pengumpulan($data['ngumpul']->id_jenis)->mk == 'ya'){
            $data['mk'] = $this->libdb->get_all_mk();
            $data['num_rows'] = $this->libdb->count_all_dosen($id);
            $data['total_ngumpul'] = $this->libdb->count_all_ngumpul($id);
        }else {
            $data['num_rows'] = $this->ngumpul_m->count_all_detail($id);
            $data['total_ngumpul'] = $this->ngumpul_m->count_all_ngumpul($id);
        }
        $this->load->view('admin/detngumpul_excel', $data);
    }

    public function soalnilai($idrule,$npp,$kodemk)
    {
    	$this->ngumpul_m->soalnilai($idrule,$npp,$kodemk);
    	$this->session->set_flashdata('type','success');
    	$this->session->set_flashdata('pesan','Data Berhasil disimpan');
    	redirect('admin/ngumpul/detail/'.$idrule);
    }
}