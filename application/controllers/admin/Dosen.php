<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dosen extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/dosen_m');
    }

    function index($offset = '') 
    {
        $this->session->unset_userdata('cari_dosen','');
        
    	$limit = 10;
    	$data['title'] = "List Dosen";
    	$data['main'] = "admin/tdosen_v";
    	$data['dosen'] = $this->dosen_m->get_all($limit,$offset);
        $data['num_rows'] = $this->dosen_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/dosen/index/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }

    public function cari($offset = '')
    {
    	if($this->input->post('cari_dosen')){
    		$this->session->set_userdata('cari_dosen',$this->input->post('cari_dosen'));
    	}

    	$limit = 10;
    	$data['title'] = "List Dosen";
    	$data['main'] = "admin/tdosen_v";
    	$data['dosen'] = $this->dosen_m->get_all($limit,$offset);
        $data['num_rows'] = $this->dosen_m->count_all();

    	$data['pagination'] = pagination(site_url('admin/dosen/cari/'),$data['num_rows'],$limit,4);

    	$this->load->view('admin/main_v', $data);
    }
}