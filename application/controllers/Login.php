<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = "Login Administrator";
		$this->load->view('login_v', $data);
	}

	public function login()
	{
		$this->load->model('login_m');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		if($cek = $this->login_m->cek_login($username,$password)){
			//tambah session disini
			$this->session->set_userdata('npp',$cek->npp);
			$this->session->set_userdata('status',$cek->status);
			$this->session->set_userdata('is_dosen_login',true);

			redirect('main');
		} else {
			$this->session->set_flashdata('test', 'ID Atau Password anda SALAH');
			redirect('login');
		}
	}

	public function logout()
	{
		$this->session->unset_userdata('npp');
		$this->session->unset_userdata('status');
		$this->session->unset_userdata('is_dosen_login');
		redirect('');
	}

}