<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ngumpul extends Dosen_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ngumpul_m');
    }

    function mk($offset = 0) {
        $this->session->unset_userdata('cari_nilai','');

    	$limit = 10;
    	$data['title'] = "List Pengumpulan";
    	$data['main'] = 'tngumpul_v';
    	$data['status'] = "Dengan Matakuliah";
    	$data['nilai'] = $this->ngumpul_m->get_all($limit,$offset,'ya');
    	$data['num_rows'] = $this->ngumpul_m->count_all('ya');
    	$data['pagination'] = pagination(site_url('nilai/index/'),$data['num_rows'],$limit,3);
        $this->load->view('main_v', $data);
    }

    function tidak($offset = 0) {
        $this->session->unset_userdata('cari_nilai','');

    	$limit = 10;
    	$data['title'] = "List Pengumpulan";
    	$data['main'] = 'tngumpul_v';
    	$data['status'] = "Tanpa Matakuliah";
    	$data['nilai'] = $this->ngumpul_m->get_all($limit,$offset,'tidak');
    	$data['num_rows'] = $this->ngumpul_m->count_all('tidak');
    	$data['pagination'] = pagination(site_url('nilai/index/'),$data['num_rows'],$limit,3);
        $this->load->view('main_v', $data);
    }

    public function carimk($offset = '')
    {
        if($this->input->post('cari_nilai')){
            $this->session->set_userdata('cari_nilai',$this->input->post('cari_nilai'));
        }

        $limit = 10;
        $data['title'] = "List Nilai";
        $data['main'] = "tngumpul_v";
        $data['status'] = "Dengan Matakuliah";
        $data['nilai'] = $this->ngumpul_m->get_all($limit,$offset,'ya');
        $data['num_rows'] = $this->ngumpul_m->count_all('ya');

        $data['pagination'] = pagination(site_url('nilai/cari/'),$data['num_rows'],$limit,3);

        $this->load->view('main_v', $data);
    }

    public function caritidak($offset = '')
    {
        if($this->input->post('cari_nilai')){
            $this->session->set_userdata('cari_nilai',$this->input->post('cari_nilai'));
        }

        $limit = 10;
        $data['title'] = "List Nilai";
        $data['main'] = "tngumpul_v";
        $data['status'] = "Tanpa Matakuliah";
        $data['nilai'] = $this->ngumpul_m->get_all($limit,$offset,'tidak');
        $data['num_rows'] = $this->ngumpul_m->count_all('tidak');

        $data['pagination'] = pagination(site_url('nilai/cari/'),$data['num_rows'],$limit,3);

        $this->load->view('main_v', $data);
    }
}