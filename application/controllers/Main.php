<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends Dosen_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['title'] = "Dashboard Sistem";
		$data['main'] = "dashboard_v";
		$data['acara'] = $this->libdb->get_jadwal_acara($this->session->userdata('npp'));
		$data['ngumpul'] = $this->libdb->get_jadwal_ngumpul($this->session->userdata('npp'));
		$this->load->view('main_v', $data);
	}

}

/* End of file Main.php */
/* Location: ./application/controllers/Main.php */