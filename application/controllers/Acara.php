<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Acara extends Dosen_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('acara_m');
    }

    function index($offset = 0) {
        $this->session->unset_userdata('cari_acara','');

    	$limit = 10;
    	$data['title'] = "List Acara";
    	$data['main'] = 'tacara_v';
    	$data['acara'] = $this->acara_m->get_all($limit,$offset);
    	$data['num_rows'] = $this->acara_m->count_all();
    	$data['pagination'] = pagination(site_url('acara/index/'),$data['num_rows'],$limit,3);
        $this->load->view('main_v', $data);
    }

    public function cari($offset = '')
    {
        if($this->input->post('cari_acara')){
            $this->session->set_userdata('cari_acara',$this->input->post('cari_acara'));
        }

        $limit = 10;
        $data['title'] = "List Acara";
        $data['main'] = "tacara_v";
        $data['acara'] = $this->acara_m->get_all($limit,$offset);
        $data['num_rows'] = $this->acara_m->count_all();

        $data['pagination'] = pagination(site_url('acara/cari/'),$data['num_rows'],$limit,3);

        $this->load->view('main_v', $data);
    }

    public function detail($id,$offset = '')
    {
        $this->session->unset_userdata('cari_acaradet');
    	$limit = 10;
    	$data['title'] = "List Acara";
    	$data['main'] = 'detacara_v';
    	$data['detail'] = $this->acara_m->get_one($id);
    	$data['acara'] = $this->acara_m->get_all_detail($limit,$offset,$id);
    	$data['num_rows'] = $this->acara_m->count_all_detail($id);
    	$data['pagination'] = pagination(site_url('acara/detail/'.$id.'/'."/"),$data['num_rows'],$limit,4);
        $this->load->view('main_v', $data);
    }

     public function caridetail($id,$offset = '')
    {
    	if($this->input->post('cari_acaradet')){
            $this->session->set_userdata('cari_acaradet',$this->input->post('cari_acaradet'));
        }

    	$limit = 10;
    	$data['title'] = "List Acara";
    	$data['main'] = 'detacara_v';
    	$data['acara'] = $this->acara_m->get_all_detail($limit,$offset,$id);
    	$data['detail'] = $this->acara_m->get_one($id);
    	$data['num_rows'] = $this->acara_m->count_all_detail($id);
    	$data['pagination'] = pagination(site_url('acara/cari/'.$id.'/'."/"),$data['num_rows'],$limit,4);
        $this->load->view('main_v', $data);
    }
}