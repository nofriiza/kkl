<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acara_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit,$offset)
	{
		$this->db->select('* ,(SELECT id from acara b where b.id = acara.id) as id_acara');
		if($limit){
			if($offset){
				$this->db->limit($limit,$ofset);
			} else {
				$this->db->limit($limit);
			}
		}
		$this->db->from('acara');
		$this->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');
		if($this->session->userdata('cari_acara')){
			$this->db->join('maspegawai', 'maspegawai.npp = peserta_acara.npp', 'INNER');
			$this->db->like('maspegawai.nama', $this->session->userdata('cari_acara'));
		}
		$this->db->where('npp', $this->session->userdata('npp'));
		$this->db->order_by('selesai', 'desc');
		return $this->db->get();
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('acara')->row();
	}

	public function get_all_detail($limit,$offset,$id)
	{
		if($limit){
			if($offset){
				$this->db->limit($limit,$offset);
			} else {
				$this->db->limit($limit);
			}
		}
		$this->db->from('acara');
		$this->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');

		if($this->session->userdata('cari_acaradet')){
			$this->db->join('maspegawai', 'maspegawai.npp = peserta_acara.npp', 'INNER');
			$this->db->like('maspegawai.nama', $this->session->userdata('cari_acaradet'));
		}
		$this->db->where('id_acara', $id);
		$this->db->order_by('selesai', 'desc');
		return $this->db->get();
	}

	public function count_all()
	{
		$this->db->from('acara');
		$this->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');
		if($this->session->userdata('cari_acara')){
			$this->db->join('maspegawai', 'maspegawai.npp = peserta_acara.npp', 'INNER');
			$this->db->like('maspegawai.nama', $this->session->userdata('cari_acara'));
		}
		$this->db->where('npp', $this->session->userdata('npp'));
		return $this->db->count_all_results();
	}

	public function count_all_detail($id)
	{
		$this->db->from('acara');
		$this->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');
		if($this->session->userdata('cari_acaradet')){
			$this->db->join('maspegawai', 'maspegawai.npp = peserta_acara.npp', 'INNER');
			$this->db->like('maspegawai.nama', $this->session->userdata('cari_acaradet'));
		}
		$this->db->where('id_acara', $id);
		return $this->db->count_all_results();
	}

}

/* End of file Depanacara_m.php */
/* Location: ./application/models/Depanacara_m.php */