<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ngumpul_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit,$offset,$id)
	{
		if($limit){
			if($offset){
				$this->db->limit($limit,$ofset);
			} else {
				$this->db->limit($limit);
			}
		}
		if($this->session->userdata('cari_nilai')){
			$this->db->like('judul',$this->session->userdata('cari_nilai'));
		}
		$this->db->from('ngumpul');
		if($id){
			$this->db->join('jenis_ngumpul', 'jenis_ngumpul.id = ngumpul.id_jenis', 'INNER');
			$this->db->where('mk', $id);
			if($id == 'tidak'){
				$this->db->join('detngumpul', 'detngumpul.id_ngumpul = ngumpul.id', 'INNER');
				$this->db->where('detngumpul.npp', $this->session->userdata('npp'));
			}
		}
		return $this->db->get();
	}

	public function get_ngumpul($id,$npp)
	{
		$this->db->where('id_ngumpul', $id);
		$this->db->where('npp', $npp);
		return $this->db->get('detngumpul')->row();
	}

	public function count_all($id)
	{
		if($this->session->userdata('cari_nilai')){
			$this->db->like('judul',$this->session->userdata('cari_nilai'));
		}
		$this->db->from('ngumpul');
		if($id){
			$this->db->join('jenis_ngumpul', 'jenis_ngumpul.id = ngumpul.id_jenis', 'INNER');
			$this->db->where('mk', $id);
			if($id == 'tidak'){
				$this->db->join('detngumpul', 'detngumpul.id_ngumpul = ngumpul.id', 'INNER');
				$this->db->where('detngumpul.npp', $this->session->userdata('npp'));
			}
		}
		return $this->db->count_all_results();
	}

}