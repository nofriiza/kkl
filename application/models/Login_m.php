<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function cek_login($username,$password)
	{
		$this->db->where('username', $username);
		$this->db->or_where('npp',$username);
		$this->db->where('password', $password);
		$this->db->where('status', "dosen");
		return $this->db->get('login')->row();
	}

}

/* End of file Depanacara_m.php */
/* Location: ./application/models/Depanacara_m.php */