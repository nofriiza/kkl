<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ngumpul_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		$this->db->where('thajaran', $this->libdb->get_thajaran_aktif());
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_ngumpul')){
			$this->db->like('judul', $this->session->userdata('cari_ngumpul'));
		}		
		$this->db->from('ngumpul');		
		$this->db->order_by('id', 'desc');
		return $this->db->get();
	}

	public function get_all_detail($limit1 = '',$limit2 = '',$id = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_ngumpul')){
			$this->db->like('judul', $this->session->userdata('cari_ngumpul'));
		}
		$this->db->where('id_ngumpul', $id);
		$this->db->from('detngumpul');		
		$this->db->order_by('nilai', 'asc');
		return $this->db->get();
	}

	public function count_all()
	{
		if($this->session->userdata('cari_ngumpul')){
			$this->db->like('judul', $this->session->userdata('cari_ngumpul'));
		}
		return $this->db->count_all_results('ngumpul');
	}

	public function count_all_detail($id)
	{
		if($this->session->userdata('cari_ngumpul')){
			$this->db->like('judul', $this->session->userdata('cari_ngumpul'));
		}
		$this->db->where('id_ngumpul', $id);
		return $this->db->count_all_results('detngumpul');
	}

	public function count_all_ngumpul($id)
	{
		$this->db->where('id_ngumpul', $id);
		$this->db->where('tgl_dikumpul !=', '0000-00-00');
		return $this->db->count_all_results('detngumpul');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('ngumpul')->row();
	}

	public function get_one_detail($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('detngumpul')->row();
	}

	public function get_one_data($id_rule,$npp,$kodemk)
	{
		$this->db->where('npp', $npp);
		$this->db->where('kodemk', $kodemk);
		$this->db->where('id_ngumpul', $id_rule);
		return $this->db->get('detngumpul')->row();
	}

	public function get_dosen_tambah($id)
	{
		$thajaran = $this->libdb->get_thajaran_aktif();

		$this->db->where('id_ngumpul', $id);
		$detail = $this->db->get('detngumpul');


		$this->db->select('*');
		$this->db->from('maspegawai');
		$this->db->where('keaktifan', "Aktif");
		foreach ($detail->result() as $rows) {
			if(@$rows->npp){
				$this->db->where('npp !=', $rows->npp);
			}
		}
		$this->db->group_by('npp');
		$this->db->order_by('nama');
		return $this->db->get();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('ngumpul');

		$this->db->where('id_ngumpul', $id);
		$this->db->delete('peserta_ngumpul');
	}

	public function simpan()
	{
		$data = array(
			'id_jenis' => $this->input->post('id_jenis'),
			'judul'	=> $this->input->post('judul'),
			'waktu' => tgl_sql($this->input->post('waktu'))
		);

		if($this->input->post('idngumpul')){
			$this->db->where('id', $this->input->post('idngumpul'));
			$this->db->update('ngumpul', $data);
		} else {
			$data['thajaran'] = $this->libdb->get_thajaran_aktif();
			$this->db->insert('ngumpul', $data);
			$id = $this->db->insert_id();


			if($this->input->post('pilih') == 'pilih'){
				foreach ($this->input->post('npp') as $rows) {
					$peserta = array(
						'id_ngumpul' 	=> $id,
						'npp' 		=> $rows 
					);
					$this->db->insert('detngumpul', $peserta);
				}
			} elseif($this->input->post('pilih') == 'semua'){
				$this->db->select('*');
				$this->db->from('maspegawai');
				$this->db->where('keaktifan','Aktif');
				if($this->input->post('option') == 'dosen'){
					$this->db->where("(statuspegawai = 'Dosen Tetap Yayasan' or statuspegawai = 'Dosen PK' or statuspegawai = 'Dosen Tidak Tetap')");
				} elseif($this->input->post('option') == 'pegawai'){
					$this->db->where("(statuspegawai = 'Pegawai Kontrak' or statuspegawai = 'Pegawai Tetap')");
				}
				$peserta = $this->db->get();
				foreach ($peserta->result() as $rows) {
					$data = array(
						'id_ngumpul' 	=> $id,
						'npp'		=> $rows->npp
					);
					$this->db->insert('detngumpul',$data);
				}
			}
			
			/*if($this->input->post('npp')){
				$id = $this->db->insert_id();
				foreach ($this->input->post('npp') as $rows) {
					$data2 = array(
						'id_ngumpul' 	=> $id,
						'npp'			=> $rows 
					);
					$this->db->insert('detngumpul', $data2);
				}
			}*/
		}
	}

	public function save()
	{
		$data = array(
			'id_ngumpul' => $this->input->post('id_ngumpul'), 
			'npp' => $this->input->post('npp') 
		);
		$this->db->insert('detngumpul', $data);
		return $this->input->post('id_ngumpul');
	}

	public function ngumpul()
	{
		$ngumpul = $this->get_one($this->input->post('id_ngumpul'));
		$this->db->where('id_jenis', $ngumpul->id_jenis);
		$this->db->order_by('waktu', 'desc');
		$this->db->join('detrule', 'rule.id = detrule.id_rule','INNER');
		$rule = $this->db->get('rule');
		$deadline = new DateTime($ngumpul->waktu);
        $sekarang = new DateTime(date("Y-m-d"));
        $beda = $sekarang->diff($deadline);
        if($sekarang <= $deadline){
        	$nilai = 5;
        } else {
        	foreach ($rule->result() as $rows) {
        		if($beda->format("%d") <= $rows->waktu ){
        			$nilai = $rows->nilai;
        		}
        	}
        }

		$data = array(
			'keterangan' => $this->input->post('keterangan'), 
			'nilai' => $nilai,
			'tgl_dikumpul' => $sekarang->format("Y-m-d")
		);
		$this->db->where('id', $this->input->post('id_detngumpul'));
		$this->db->update('detngumpul', $data);
		return $this->input->post('id_ngumpul');
	}

	public function soalnilai($idrule,$npp,$kodemk)
	{
		$ngumpul = $this->get_one($idrule);
		$this->db->where('id_jenis', $ngumpul->id_jenis);
		$this->db->order_by('waktu', 'desc');
		$this->db->join('detrule', 'rule.id = detrule.id_rule','INNER');
		$rule = $this->db->get('rule');
		$deadline = new DateTime($ngumpul->waktu);
        $sekarang = new DateTime(date("Y-m-d"));
        $beda = $sekarang->diff($deadline);
        if($sekarang <= $deadline){
        	$nilai = 5;
        } else {
        	foreach ($rule->result() as $rows) {
        		if($beda->format("%d") <= $rows->waktu ){
        			$nilai = $rows->nilai;
        			break;
        		} else {
        			$nilai = 0;
        		}
        	}
        }
        $data = array(
        	'id_ngumpul' => $idrule, 
        	'npp' => $npp, 
        	'kodemk' => $kodemk,
        	'tgl_dikumpul' => $sekarang->format("Y-m-d"), 
        	'nilai' => $nilai 
        );

        $this->db->insert('detngumpul', $data);
	}

}

/* End of file ngumpulngumpul_m.php */
/* Location: ./application/models/ngumpulngumpul_m.php */