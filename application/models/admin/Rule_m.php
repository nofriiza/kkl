<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rule_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_rule')){
			$this->db->like('id_jenis', $this->session->userdata('cari_rule'));
		}
		$this->db->from('rule');
		return $this->db->get();
	}

	public function get_detail($id)
	{
		$this->db->where('id_rule', $id);
		$this->db->order_by('waktu', 'asc');
		return $this->db->get('detrule');
	}

	public function count_all()
	{
		if($this->session->userdata('cari_rule')){
			$this->db->like('nama_rule', $this->session->userdata('cari_rule'));
		}
		return $this->db->count_all_results('rule');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('rule')->row();
	}

	public function get_one_detail($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('detrule')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rule');
	}

	public function simpan()
	{
		$data = array(
			'id_jenis' => $this->session->userdata('rule_idjenis')
		);
		$this->db->insert('rule', $data);
		$id = $this->db->insert_id();
		foreach ($this->cart->contents() as $rows){
			$waktu = tgl_sql($rows['option']['waktu']);
			$data2 = array(
				'id_rule' => $id,
				'waktu' => str_replace('-', '', $waktu),
				'nilai' => $rows['id']
			);
			$this->db->insert('detrule', $data2);
		}
	}

	public function save()
	{
		$waktu = $this->input->post('waktu');
		$data = array(
			'waktu' => $waktu,
			'nilai' => $this->input->post('nilai') 
		);
		$this->db->where('id', $this->input->post('iddetrule'));
		$this->db->update('detrule', $data);
	}

}

/* End of file rulerule_m.php */
/* Location: ./application/models/rulerule_m.php */