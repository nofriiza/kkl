<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peserta_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($id)
	{
		$this->db->where('id_acara', $id);
        $this->db->join('maspegawai', 'maspegawai.npp = peserta_acara.npp');
        $this->db->order_by('maspegawai.nama', 'asc');        
		return $this->db->get('peserta_acara');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('peserta_acara')->row();
	}

	public function get_one_dosen($id)
	{
		$this->db->where('npp', $id);
        $this->db->where('keaktifan', 'Aktif');
		return $this->db->get('maspegawai')->row();
	}

	public function get_dosen_input($id = '')
    {
        if($id){
            $this->db->where('id_acara', $id);
            $peserta = $this->db->get('peserta_acara');



            foreach ($peserta->result() as $rows) {
                $this->db->where('npp !=', $rows->npp);
            }
        }
        $this->db->from('maspegawai');
        $this->db->where('keaktifan', "Aktif");
        $this->db->order_by('nama', 'asc');
        return $this->db->get();
    }

    public function get_dosens_input($id = '')
    {
        if($id){
            $this->db->where('id_acara', $id);
            $peserta = $this->db->get('peserta_acara');



            foreach ($peserta->result() as $rows) {
                $this->db->where('npp !=', $rows->npp);
            }
        }
        $this->db->from('maspegawai');
        $this->db->where('keaktifan', "Aktif");
        $this->db->where("(statuspegawai = 'Dosen Tetap Yayasan' or statuspegawai = 'Dosen PK' or statuspegawai = 'Dosen Tidak Tetap')");
        $this->db->order_by('nama', 'asc');

        return $this->db->get();
    }

    public function get_pegawai_input($id = '')
    {
        if($id){
            $this->db->where('id_acara', $id);
            $peserta = $this->db->get('peserta_acara');

            foreach ($peserta->result() as $rows) {
                $this->db->where('npp !=', $rows->npp);
            }
        }
        $this->db->from('maspegawai');
        $this->db->where('keaktifan', "Aktif");
        $this->db->where("(statuspegawai = 'Pegawai Kontrak' or statuspegawai = 'Pegawai Tetap')");
        $this->db->order_by('nama', 'asc');
        return $this->db->get();
    }

    public function simpan()
    {
    	$data = array(
    		'id_acara' => $this->input->post('id_acara'), 
    		'npp' => $this->input->post('npp')
    	);

    	if($this->input->post('id_peserta')){
    		$this->db->where('id', $this->input->post('id_peserta'));
    		$this->db->update('peserta_acara', $data);
    	} else{
    		$this->db->insert('peserta_acara', $data);
    	}
    }

    public function save($id = '',$izin = '')
    {
    	$acara = $this->libdb->get_one_acara($this->session->userdata('idacara'));
    	$jenis = $acara->id_jenis;
        $mulai = new DateTime($acara->mulai);
        $selesai = new DateTime($acara->selesai);
        $sekarang = new DateTime($this->input->post('jam_datang') ? $this->input->post('jam_datang') : date("Y-m-d H:i:s"));
        $beda = $sekarang->diff($mulai);
        /*echo ($sekarang >= $selesai) ? 'ya' : 'ga' ;
        echo $sekarang->format("Y-m-d H:i:s").'<br />';
        echo $beda->format("%Y-%m-%d %H:%i:%s").'<br />';*/

        $this->db->select('*');        
        $this->db->from('rules');
        $this->db->join('detrules', 'rules.id = detrules.id_rule', 'INNER');
    	$this->db->where('id_jenis', $jenis);
    	$this->db->order_by('waktu', 'desc');
    	$rule = $this->db->get();

    	if($beda->format("%d") > 1){
            //echo "kesini 3".'<br />';
    		$nilai = 0;
    	} elseif($sekarang >= $selesai || $izin) {
            //echo "kesini 4 ".'<br />';
    		$nilai = 0;
            $keterangans = "Tidak Hadir Tanpa Keterangan";
    	} elseif($sekarang <= $mulai){
            $nilai = 5;
            $keterangans = "Hadir";
        } else {
            //echo "kesini".'<br />';
            if($sekarang >= $mulai){
                foreach ($rule->result() as $rows) {
                    if($beda->format('%H:%I:%S') >= $rows->waktu){
                        $nilai = $rows->nilai;
                        $keterangans = "Hadir";
                        break;
                    }
                }
            } else {
                //echo "kesini 2 ".'<br />';
                $nilai = 0;
                $keterangans = "Tidak Hadir Tanpa Keterangan";
            }   
        }

        $keterangan = '';
        if($this->input->post('keterangan')){
            if($izin){
                $keterangan .= 'Izin,';
            }
            $keterangan .= $this->input->post('keterangan'); 
        } else {
            $keterangan = $keterangans;
        }
        /*$keterangan = ($this->input->post('keterangan')) ? $this->input->post('keterangan') : $keterangans ;*/
        $jam_datang = ($this->input->post('jam_datang')) ? $this->input->post('jam_datang') : date("Y-m-d H:i:s");
        //$nilai = ($this->input->post('nilai')) ? $this->input->post('nilai') : $nilai;
        //echo $nilai.'<br />';
        //echo $jam_datang.'<br />';
        //echo $keterangan.'<br />';
        //die();

    	$data = array(
    		'nilai'			=> $nilai,
    		'jam_datang'	=> $jam_datang,
    		'keterangan' 	=> $keterangan 
    	);
    	if($id){
    		$this->db->where('id', $id);
    	} else{
    		$this->db->where('id', $this->input->post('id_peserta'));
    	}
    	$this->db->update('peserta_acara', $data);

    }

    public function selesai()
    {
        $data = array(
            'notulen' => $this->input->post('notulensi') 
        );
        if($this->input->post('jam_diakhiri') == '0000-00-00 00:00:00'){
            /*$this->db->where('id',$this->input->post('id_acara'));
            $data3 = array(
                'jam_diakhiri' => date("Y-m-d H:i:s")
            );
            $this->db->update('acara',$data3);*/
            $data['jam_diakhiri'] = date("Y-m-d H:i:s");
        }
        $this->db->where('id', $this->input->post('id_acara'));
        $this->db->update('acara', $data);

        $this->db->where('jam_datang', "0000-00-00 00:00:00");
        $this->db->where('id_acara', $this->input->post('id_acara'));
        $peserta = $this->db->get('peserta_acara');

        /*$keterangan = "Tidak Hadir Tanpa Keterangan";
        $data2 = array(
            'nilai'         => 0,
            'jam_datang'    => date("Y-m-d H:i:s"),
            'keterangan'    => $keterangan 
        );*/
        foreach ($peserta->result() as $rows) {
            /*$this->db->where('id', $rows->id);
            $this->db->update('peserta_acara', $data2);*/
            $this->save($rows->id);
        }
    }

    public function hapus($id)
    {
    	$this->db->where('id', $id);
    	$this->db->delete('peserta_acara');
    }

}

/* End of file Peserta_m.php */
/* Location: ./application/models/Peserta_m.php */