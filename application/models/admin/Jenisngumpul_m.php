<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenisngumpul_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_jenisngumpul')){
			$this->db->like('nama_jenisngumpul', $this->session->userdata('cari_jenisngumpul'));
		}
		$this->db->from('jenis_ngumpul');
		return $this->db->get();
	}

	public function count_all()
	{
		if($this->session->userdata('cari_jenisngumpul')){
			$this->db->like('nama_jenisngumpul', $this->session->userdata('cari_jenisngumpul'));
		}
		return $this->db->count_all_results('jenis_ngumpul');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('jenis_ngumpul')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('jenis_ngumpul');
	}

	public function simpan()
	{
		$data = array(
			'nama_jenis' => $this->input->post('nama_jenis') 
		);

		if($this->input->post('idjenisngumpul')){
			$this->db->where('id', $this->input->post('idjenisngumpul'));
			$this->db->update('jenis_ngumpul', $data);
		} else {
			$this->db->insert('jenis_ngumpul', $data);
		}
	}

}

/* End of file Jenisacara_m.php */
/* Location: ./application/models/Jenisacara_m.php */