<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rules_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_rules')){
			$this->db->like('id_jenis', $this->session->userdata('cari_rules'));
		}
		$this->db->from('rules');
		return $this->db->get();
	}

	public function get_detail($id)
	{
		$this->db->where('id_rule', $id);
		$this->db->order_by('waktu', 'asc');
		return $this->db->get('detrules');
	}

	public function count_all()
	{
		if($this->session->userdata('cari_rules')){
			$this->db->like('nama_rules', $this->session->userdata('cari_rules'));
		}
		return $this->db->count_all_results('rules');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('rules')->row();
	}

	public function get_one_detail($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('detrules')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('rules');
	}

	public function simpan()
	{
		$data = array(
			'id_jenis' => $this->session->userdata('rules_idjenis')
		);
		$this->db->insert('rules', $data);
		$id = $this->db->insert_id();
		foreach ($this->cart->contents() as $rows){
			$data2 = array(
				'id_rule' => $id,
				'waktu' => $rows['option']['waktu'],
				'nilai' => $rows['id']
			);
			$this->db->insert('detrules', $data2);
		}
	}

	public function save()
	{
		$data = array(
			'waktu' => $this->input->post('waktu'), 
			'nilai' => $this->input->post('nilai') 
		);
		$this->db->where('id', $this->input->post('iddetrule'));
		$this->db->update('detrules', $data);
	}

}

/* End of file rulerule_m.php */
/* Location: ./application/models/rulerule_m.php */