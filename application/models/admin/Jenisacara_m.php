<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenisacara_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_jenisacara')){
			$this->db->like('nama_jenisacara', $this->session->userdata('cari_jenisacara'));
		}
		$this->db->from('jenis_acara');
		return $this->db->get();
	}

	public function count_all()
	{
		if($this->session->userdata('cari_jenisacara')){
			$this->db->like('nama_jenisacara', $this->session->userdata('cari_jenisacara'));
		}
		return $this->db->count_all_results('jenis_acara');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('jenis_acara')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('jenis_acara');
	}

	public function simpan()
	{
		$data = array(
			'nama_jenis' => $this->input->post('nama_jenis') 
		);

		if($this->input->post('idjenisacara')){
			$this->db->where('id', $this->input->post('idjenisacara'));
			$this->db->update('jenis_acara', $data);
		} else {
			$this->db->insert('jenis_acara', $data);
		}
	}

}

/* End of file Jenisacara_m.php */
/* Location: ./application/models/Jenisacara_m.php */