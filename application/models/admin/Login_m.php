<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function ceklogin($username,$password)
	{
		$this->db->where('username', $username);
		$this->db->or_where('npp',$username);
		$this->db->where('password', $password);
		return $this->db->get('login')->row();
	}

}

/* End of file Login_m.php */
/* Location: ./application/models/Login_m.php */