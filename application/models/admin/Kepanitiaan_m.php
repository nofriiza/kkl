<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kepanitiaan_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_kepanitiaan')){
			$this->db->like('nama_kepanitiaan', $this->session->userdata('cari_kepanitiaan'));
		}
		$this->db->order_by('id', 'desc');
		$this->db->from('kepanitiaan');
		return $this->db->get();
	}

	public function get_all_detail($id)
	{
		return $this->db->where('id_kepanitiaan', $id)->get('det_kepanitiaan');
	}

	public function count_all()
	{
		if($this->session->userdata('cari_kepanitiaan')){
			$this->db->like('nama_kepanitiaan', $this->session->userdata('cari_kepanitiaan'));
		}
		return $this->db->count_all_results('kepanitiaan');
	}

	public function count_all_detail($id)
	{
		return $this->db->count_all_results('det_kepanitiaan');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('kepanitiaan')->row();
	}

	public function get_one_detail($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('det_kepanitiaan')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('kepanitiaan');

		$this->db->where('id_kepanitiaan', $id);
		$this->db->delete('det_kepanitiaan');
	}

	public function hapusdetail($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('det_kepanitiaan');
	}

	public function simpan()
	{
		$data = array(
			'nama_kepanitiaan' => $this->input->post('nama_kepanitiaan'),
			'thajaran' => $this->input->post('thajaran'),
			'tgl_sk' => tgl_sql($this->input->post('tgl_sk')),
			'ketua' => $this->input->post('ketua')
		);
		if($this->input->post('idkepanitiaan')){
			$this->db->where('id',$this->input->post('idkepanitiaan'));
			$this->db->update('kepanitiaan', $data);
		} else {
			$this->db->insert('kepanitiaan',$data);
			$id = $this->db->insert_id();

			$data2 = array(
				'id_kepanitiaan' => $id,
				'npp' => $this->input->post('ketua'),
				'nilai' => 0,
				'keterangan' => "Ketua Kepanitiaan"
			);
			$this->db->insert('det_kepanitiaan',$data2);
		}
	}

	public function simpandetail()
	{
		$idkepanitiaan = $this->input->post('idkepanitiaan');
		$data = array(
			'id_kepanitiaan' => $idkepanitiaan, 
			'npp' => $this->input->post('npp'), 
			'nilai' => 0, 
			'keterangan' => $this->input->post('keterangan') 
		);
		if($this->input->post('iddetkepanitiaan')){
			$data['nilai'] = $this->input->post('nilai');
			$this->db->where('id', $this->input->post('iddetkepanitiaan'));
			$this->db->update('det_kepanitiaan', $data);
		} else {
			$this->db->insert('det_kepanitiaan',$data);
		}
	}

    
}
        