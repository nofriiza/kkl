<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acara_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_acara')){
			$this->db->like('judul', $this->session->userdata('cari_acara'));
		}
		$this->db->order_by('id', 'desc');
		$this->db->from('acara');
		return $this->db->get();
	}

	public function count_all()
	{
		if($this->session->userdata('cari_acara')){
			$this->db->like('judul', $this->session->userdata('cari_acara'));
		}
		return $this->db->count_all_results('acara');
	}

	public function get_one($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('acara')->row();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('acara');

		$this->db->where('id_acara', $id);
		$this->db->delete('peserta_acara');
	}

	public function simpan()
	{
		$data = array(
			'id_jenis' => $this->input->post('id_jenis'),
			'judul' => $this->input->post('judul'),
			'mulai' => $this->input->post('mulai'),
			'selesai' => $this->input->post('selesai'),
			'tempat' => $this->input->post('tempat'),
			'keterangan' => $this->input->post('keterangan'),
			'created_by' => $this->session->userdata('npp'),
			'thajaran' => $this->input->post('thajaran')
		);

		if($this->input->post('idacara')){
			$this->db->where('id', $this->input->post('idacara'));
			$this->db->update('acara', $data);
		} else {
			$this->db->insert('acara', $data);
			$id = $this->db->insert_id();
			if($this->input->post('pilih') == 'pilih'){
				foreach ($this->input->post('npp') as $rows) {
					$peserta = array(
						'id_acara' 	=> $id,
						'npp' 		=> $rows 
					);
					$this->db->insert('peserta_acara', $peserta);
				}
			} elseif($this->input->post('pilih') == 'semua'){
				$this->db->select('*');
				$this->db->from('maspegawai');
				$this->db->where('keaktifan','Aktif');
				if($this->input->post('option') == 'dosen'){
					$this->db->where("(statuspegawai = 'Dosen Tetap Yayasan' or statuspegawai = 'Dosen PK' or statuspegawai = 'Dosen Tidak Tetap')");
				} elseif($this->input->post('option') == 'pegawai'){
					$this->db->where("(statuspegawai = 'Pegawai Kontrak' or statuspegawai = 'Pegawai Tetap')");
				}
				$peserta = $this->db->get();
				foreach ($peserta->result() as $rows) {
					$data = array(
						'id_acara' 	=> $id,
						'npp'		=> $rows->npp
					);
					$this->db->insert('peserta_acara',$data);
				}
			}
		}
	}
}

/* End of file acaraacara_m.php */
/* Location: ./application/models/acaraacara_m.php */