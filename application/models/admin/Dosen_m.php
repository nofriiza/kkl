<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen_m extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_all($limit1 = '',$limit2 = '')
	{
		if($limit1){
			if($limit2){
				$this->db->limit($limit1,$limit2);
			} else {
				$this->db->limit($limit1);
			}
		}
		if($this->session->userdata('cari_dosen')){
			$this->db->like('nama', $this->session->userdata('cari_dosen'));
			$this->db->or_like('npp', $this->session->userdata('cari_dosen'));
			$this->db->or_like('keaktifan', $this->session->userdata('cari_dosen'));

		}
		$this->db->from('maspegawai');
		return $this->db->get();
	}

	public function count_all()
	{
		if($this->session->userdata('cari_dosen')){
			$this->db->like('nama', $this->session->userdata('cari_dosen'));
			$this->db->or_like('npp', $this->session->userdata('cari_dosen'));
			$this->db->or_like('keaktifan', $this->session->userdata('cari_dosen'));
		}
		return $this->db->count_all_results('maspegawai');
	}
}