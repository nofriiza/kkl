<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	if ( ! function_exists("pagination"))
	{
		function pagination($base_url, $total_rows, $per_page, $segment = 3)
		{
			$CI =& get_instance();
			
			$config['base_url'] = $base_url;
			$config['total_rows'] = $total_rows;
			$config['per_page'] = $per_page;
			$config['uri_segment'] = $segment;
			$config['full_tag_open'] = '<div class="box-footer clearfix"><ul class="pagination pagination-sm no-margin pull-right">';
    		$config['full_tag_close'] = '</ul></div>';
    		$config['first_link'] = 'First';
	    	$config['first_tag_open'] = '<li>';
	    	$config['first_tag_close'] = '</li>';
	    	$config['last_link'] = 'Last';
	    	$config['last_tag_open'] = '<li>';
	    	$config['last_tag_close'] = '</li>';
    		$config['next_link'] = '&raquo;';
	    	$config['next_tag_open'] = '<li>';
	    	$config['next_tag_close'] = '</li>';
	    	$config['prev_link'] = '&laquo;';
	    	$config['prev_tag_open'] = '<li>';
	    	$config['prev_tag_close'] = '</li>';
	    	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	    	$config['cur_tag_close'] = '</a></li>';
	    	$config['num_tag_open'] = '<li>';
	    	$config['num_tag_close'] = '</li>';

			$CI->load->library("pagination");
			$CI->pagination->initialize($config);

			return $CI->pagination->create_links();
		}

		function tgl_indo($tgl_awal,$text=0,$waktu=0){
			$tahun	= substr($tgl_awal,0,4);
			if($waktu){
				$waktu = substr($tgl_awal, -9);
			} else{
				$waktu = '';
			}
	        if($text){
	            $bln 	= substr($tgl_awal,5,2);
	            switch($bln){
	                case "01" :
	                    $bulan = "Januari";
	                    break;
	                case "02" :
	                    $bulan = "Februari";
	                    break;
	                case "03" :
	                    $bulan = "Maret";
	                    break;
	                case "04" :
	                    $bulan = "April";
	                    break;
	                case "05" :
	                    $bulan = "Mei";
	                    break;
	                case "06" :
	                    $bulan = "Juni";
	                    break;
	                case "07" :
	                    $bulan = "Juli";
	                    break;
	                case "08" :
	                    $bulan = "Agustus";
	                    break;
	                case "09" :
	                    $bulan = "September";
	                    break;
	                case "10" :
	                    $bulan = "Oktober";
	                    break;
	                case "11" :
	                    $bulan = "November";
	                    break;
					default :
						$bulan = "Desember";
	            }
	            $hari	= substr($tgl_awal,8,2);
	            $tgl    = $hari." ".$bulan." ".$tahun." ".$waktu;
	            return $tgl;
	        }else{
	    		$bulan 	= substr($tgl_awal,5,2);
	            $hari	= substr($tgl_awal,8,2);
	            $tgl = $hari."-".$bulan."-".$tahun." ".$waktu;
	            return $tgl;
	        }
		}

		function tgl_sql($tgl_awal){
			$tahun	= substr($tgl_awal,6,4);
			$hari 	= substr($tgl_awal,3,2);
			$bulan	= substr($tgl_awal,0,2);
			$tgl = $tahun."-".$bulan."-".$hari;
			return $tgl;
		}

		function tgl_datepicker($tgl_awal)
		{
			if($tgl_awal){
				$tahun	= substr($tgl_awal,0,4);
				$bln 	= substr($tgl_awal,5,2);
				$hari	= substr($tgl_awal,8,2);
				return $bln."/".$hari."/".$tahun;
			} else {
				return "";
			}
		}
	}
?>