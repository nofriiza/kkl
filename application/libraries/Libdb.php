<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Libdb
{
	//function Libdb()
	function __construct()
	{
		$this->CI =& get_instance();
	}

	function get_jenis_acara($limit = '')
	{
		$this->CI->db->from('jenis_acara');
		return $this->CI->db->get();
	}

	function get_jenis_pengumpulan($limit = '')
	{
		$this->CI->db->from('jenis_ngumpul');
		return $this->CI->db->get();
	}

	function get_acara()
	{
		$this->CI->db->from('acara');
		return $this->CI->db->get();
	}

	public function get_matkul_input($kodemk = '')
	{
		$thajaran = $this->get_thajaran_aktif();
		$udahada = $this->CI->db->where('thajaran',$thajaran)->get('ngumpul');		
		$this->CI->db->select('*');
		$this->CI->db->from('simkurikulum');
		$this->CI->db->join('simdosenampu', 'simdosenampu.kodemk = simkurikulum.kodemk', 'INNER');
		$this->CI->db->where('simdosenampu.thajaran', $thajaran);
		/*foreach ($udahada->result() as $rows) {
			if($kodemk){

			}else if($rows->kodemk){
				$this->CI->db->where('simdosenampu.kodemk !=', $rows->kodemk);
			}
		}*/
		return $this->CI->db->get();

	}

	public function get_mk($id)
	{
		$this->CI->db->where('kodemk', $id);
		return $this->CI->db->get('simkurikulum')->row();
	}

	public function get_all_dosen()
	{
		$this->CI->db->order_by('nama', 'asc');
		$this->CI->db->where('keaktifan', "Aktif");
		return $this->CI->db->get('maspegawai');
	}

	public function get_all_acara($npp = '')
	{
		if($npp){
			$this->CI->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');
    		$this->CI->db->where('peserta_acara.npp', $npp);
    	}
		return $this->CI->db->get('acara');
	}

	public function get_all_pengumpulan($npp = '')
	{
		if($npp){
			$this->CI->db->join('detngumpul', 'ngumpul.id = detngumpul.id_ngumpul', 'INNER');
    		$this->CI->db->where('(ngumpul.id_jenis = 1 or ngumpul.id_jenis = 2 or detngumpul.npp = '.$npp.' )');
		}
		return $this->CI->db->get('ngumpul');
	}

	public function get_all_mk($limit1 = '',$limit2 = '')
	{		
		$this->CI->db->where('simdosenampu.thajaran', $this->get_thajaran_aktif());
		$this->CI->db->join('simkurikulum', 'simkurikulum.kodemk = simdosenampu.kodemk', 'INNER');
		if($limit1){
			if($limit2){
				$this->CI->db->limit($limit1,$limit2);
			} else {
				$this->CI->db->limit($limit1);
			}
		}
		return $this->CI->db->get('simdosenampu');
	}

	public function count_all_dosen()
	{		
		$this->CI->db->where('simdosenampu.thajaran', $this->get_thajaran_aktif());
		$this->CI->db->join('simkurikulum', 'simkurikulum.kodemk = simdosenampu.kodemk', 'INNER');
		return $this->CI->db->count_all_results('simdosenampu');
	}

	public function count_all_ngumpul($id)
	{		
		$this->CI->db->where('id_ngumpul', $id);
		return $this->CI->db->count_all_results('detngumpul');
	}

	public function get_thajaran_aktif()
	{
		return $this->CI->db->where('aktif','Aktif')->get('simsetting')->row()->thajaran;
	}

	public function get_thajaran()
	{
		return $this->CI->db->get('simsetting');
	}

	function get_one_jenis_acara($id)
	{
		$this->CI->db->from('jenis_acara');
		$this->CI->db->where('id',$id);
		return $this->CI->db->get()->row();
	}

	function get_one_jenis_pengumpulan($id)
	{
		$this->CI->db->from('jenis_ngumpul');
		$this->CI->db->where('id',$id);
		return $this->CI->db->get()->row();
	}

    function get_one_dosen($id)
    {
        $this->CI->db->from('maspegawai');
        $this->CI->db->where('npp',$id);
        $this->CI->db->where('keaktifan','Aktif');
        return $this->CI->db->get()->row();
    }

    function get_one_by_mk($id)
    {
        $this->CI->db->from('simdosenampu');
        $this->CI->db->where('kodemk',$id);
        $this->CI->db->join('maspegawai', 'maspegawai.npp = simdosenampu.npp', 'INNER');
        return $this->CI->db->get()->row();
    }

    function get_one_acara($id)
    {
        $this->CI->db->from('acara');
        $this->CI->db->where('id',$id);
        return $this->CI->db->get()->row();
    }

    function get_jadwal_acara($npp = '')
    {
    	$this->CI->db->from('acara');
    	$this->CI->db->join('peserta_acara', 'acara.id = peserta_acara.id_acara', 'INNER');
    	$this->CI->db->group_by('peserta_acara.id_acara');
    	$this->CI->db->order_by('mulai','desc');
    	$this->CI->db->where('left(mulai,10) >=', date("Y-m-d"));
		$this->CI->db->where('acara.jam_diakhiri', '0000-00-00 00:00:00');
    	if($npp){
    		$this->CI->db->where('peserta_acara.npp', $npp);
    	}
    	return $this->CI->db->get();
    }

    function get_jadwal_ngumpul($npp = '')
    {
    	$this->CI->db->from('ngumpul');
    	$this->CI->db->join('detngumpul', 'ngumpul.id = detngumpul.id_ngumpul', 'INNER');
    	$this->CI->db->group_by('detngumpul.id_ngumpul');
    	$this->CI->db->order_by('waktu','desc');
    	$this->CI->db->where('left(waktu,10) >=', date("Y-m-d"));
    	if($npp){
    		$this->CI->db->where('(ngumpul.id_jenis = 1 or ngumpul.id_jenis = 2 or detngumpul.npp = '.$npp.' )');

    	}
    	return $this->CI->db->get();
    }

    function get_admin_acara($npp = '')
    {
    	$this->CI->db->where('npp', $npp);
    	return $this->CI->db->get('admin_acara');
    }

    function get_all_admin_acara()
    {
    	return $this->CI->db->get('admin_acara');
    }

    function simpan_admin_acara()
    {
    	$data = array(
    		'npp' => $this->CI->input->post('npp') 
    	);
    	$this->CI->db->insert('admin_acara', $data);
    }

    function hapus_admin_acara($id)
    {
    	$this->CI->db->where('id', $id);
    	$this->CI->db->delete('admin_acara');
    }

    function get_nilai_acara($npp)
    {
    	$this->CI->db->select("*,avg(peserta_acara.nilai) as rata,count(id_acara) as jumacara");
    	$this->CI->db->from('acara');
    	$this->CI->db->join('peserta_acara', 'peserta_acara.id_acara = acara.id', 'INNER');
    	$this->CI->db->where('npp', $npp);
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('acara.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->group_by('npp');
    	$this->CI->db->order_by('rata', 'desc');
    	return $this->CI->db->get()->row();
    }

    function get_all_nilai_acara($npp)
    {
    	$this->CI->db->select("*");
    	$this->CI->db->from('acara');
    	$this->CI->db->join('peserta_acara', 'peserta_acara.id_acara = acara.id', 'INNER');
    	$this->CI->db->where('peserta_acara.npp', $npp);
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('acara.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->group_by('peserta_acara.id_acara');
    	return $this->CI->db->get();
    }

    function get_peserta_kepanitiaan($id)
    {
    	$this->CI->db->select('*');
    	$this->CI->db->from('maspegawai');
    	$this->CI->db->where("maspegawai.npp not in (SELECT ketua from kepanitiaan where id = $id)");
    	$this->CI->db->where("maspegawai.npp not in (SELECT npp from det_kepanitiaan where id_kepanitiaan = $id)");
    	$this->CI->db->where('keaktifan', "Aktif");
    	$this->CI->db->order_by('nama', 'asc');
		return $this->CI->db->get();
    }

    function get_nilai_laporan($npp)
    {
    	$this->CI->db->select("*,avg(detngumpul.nilai) as rata,count(id_ngumpul) as jumngumpul");
    	$this->CI->db->from('ngumpul');
    	$this->CI->db->join('detngumpul', 'detngumpul.id_ngumpul = ngumpul.id', 'INNER');
    	$this->CI->db->where('npp', $npp);
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('ngumpul.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->group_by('npp');
    	$this->CI->db->order_by('rata', 'desc');
    	return $this->CI->db->get()->row();
    }

    function get_all_nilai_ngumpul($npp)
    {
    	$this->CI->db->select("*");
    	$this->CI->db->from('ngumpul');
    	$this->CI->db->join('detngumpul', 'detngumpul.id_ngumpul = ngumpul.id', 'INNER');
    	$this->CI->db->where('npp', $npp);
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('ngumpul.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->group_by('detngumpul.id_ngumpul');
    	return $this->CI->db->get();
    }

    public function get_nilai_kepanitiaan($npp)
    {
    	$this->CI->db->select("*,avg(det_kepanitiaan.nilai) as rata,count(det_kepanitiaan.id) as jumkepanitiaan");
    	$this->CI->db->from('kepanitiaan');
    	$this->CI->db->join('det_kepanitiaan', 'det_kepanitiaan.id_kepanitiaan = kepanitiaan.id', 'INNER');
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('kepanitiaan.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->where('det_kepanitiaan.npp', $npp);
    	return $this->CI->db->get()->row();
    }

    function get_all_nilai_kepanitiaan($npp)
    {
    	$this->CI->db->select("*");
    	$this->CI->db->from('kepanitiaan');
    	$this->CI->db->join('det_kepanitiaan', 'det_kepanitiaan.id_kepanitiaan = kepanitiaan.id', 'INNER');
    	$this->CI->db->where('det_kepanitiaan.npp', $npp);
    	if($this->CI->session->userdata('thajaran_lapoan')){
    		$this->CI->db->where('kepanitiaan.thajaran', $this->CI->session->userdata('thajaran_lapoan'));
    	}
    	$this->CI->db->group_by('det_kepanitiaan.id_kepanitiaan');
    	return $this->CI->db->get();
    }
	
}
